<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    public function owner()
    {
        return $this->hasOne('App\User', 'hotel_id', 'id');
    }
}
