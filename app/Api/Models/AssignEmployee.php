<?php

namespace App\Api\Models;

class AssignEmployee
{
	private $_responseCode = 404;
	private $_status = false;
	private $_message = 'Not Found.';
	
	public function getJson(){
		return json_encode(array(
			'response_code'	=> $this->_responseCode,
			'status'		=> $this->_status,
			'message'		=> $this->_message,
		));
	}
	
	public function setResponseCode($responseCode){
		$this->_responseCode = $responseCode;
	}
	
	public function setStatus($status){
		$this->_status = $status;
	}
	
	public function setMessage($message){
		$this->_message = $message;
	}
	
	/*public function addEmployeeToList($id, $name, $email, $mobileNo, $gender, $hotelId){
		array_push($this->_employees, array(
						'id'		=> $id,
						'name'		=> $name.'',
						'email'		=> $email.'',
						'mobile_no'	=> $mobileNo.'',
						'gender'	=> $gender.'',
						'hotel_id'	=> $hotelId,
					));
	}*/
}
