<?php

namespace App\Api\Models;

class EmployeePerformance
{
	private $_responseCode = 404;
	private $_status = false;
	private $_message = 'List Not Found.';
	private $_employees = array();
	
	public function getJson(){
		return json_encode(array(
			'response_code'	=> $this->_responseCode,
			'status'		=> $this->_status,
			'message'		=> $this->_message,
			'employees'		=> $this->_employees,
		));
	}
	
	public function setResponseCode($responseCode){
		$this->_responseCode = $responseCode;
	}
	
	public function setStatus($status){
		$this->_status = $status;
	}
	
	public function setMessage($message){
		$this->_message = $message;
	}
	
	public function addEmployeePerformanceToList($id, $name, $totalTakenOrders, $totalSpentTime){
		array_push($this->_employees, array(
						'id'				=> $id,
						'name'				=> $name.'',
						'totalTakenOrders'	=> $totalTakenOrders.'',
						'totalSpentTime'	=> $totalSpentTime.'',
					));
	}
}
