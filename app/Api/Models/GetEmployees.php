<?php

namespace App\Api\Models;

class GetEmployees
{
	private $_responseCode = 404;
	private $_status = false;
	private $_message = 'List Not Found.';
	private $_employees = array();
	
	public function getJson(){
		return json_encode(array(
			'response_code'	=> $this->_responseCode,
			'status'		=> $this->_status,
			'message'		=> $this->_message,
			'employees'		=> $this->_employees,
		));
	}
	
	public function setResponseCode($responseCode){
		$this->_responseCode = $responseCode;
	}
	
	public function setStatus($status){
		$this->_status = $status;
	}
	
	public function setMessage($message){
		$this->_message = $message;
	}
	
	public function addEmployeeToList($id, $name, $email, $mobileNo, $gender, $hotelId){
		array_push($this->_employees, array(
						'id'		=> $id,
						'name'		=> $name.'',
						'email'		=> $email.'',
						'mobile_no'	=> $mobileNo.'',
						'gender'	=> $gender.'',
						'hotel_id'	=> $hotelId,
					));
	}
}
