<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FoodItem extends Model
{
    public function foodIngredients()
	{
		return $this->belongsToMany('App\FoodIngredient', 'food_item_ingredients', 'item_id', 'ingredient_id');
	}
	
	public function foodTypes()
	{
		return $this->belongsToMany('App\FoodType', 'food_type_items', 'item_id', 'type_id');
	}
	public function foodCategories()
	{
		return $this->hasMany('App\FoodCategory', 'id', 'category_id');
	}
}
