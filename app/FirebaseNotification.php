<?php

namespace App;

class FirebaseNotification
{
	
	public static function instance()
	{
		return new FirebaseNotification();
	}
	
    public function SendNotification($title, $message, $tokensArray) {

		$url = 'https://fcm.googleapis.com/fcm/send';

		$fields = array (
				'priority', 10,
				'registration_ids' => $tokensArray,
				'notification' => array (
						'title' => $title,
						'body'	=> $message,
						'sound'	=> 'Default',
				),
		);

		$headers = array (
				'Authorization: key=' . 'AAAAhyG7sYI:APA91bF9FHHzRs2pfRY2nDAOCq5trVlRK0RNZwK3qxB6R5FaIJDSs1RqEJiFu0AOvZJwoUUi-m1aP1wC76GFcSqo7_zdpdhDxL2lCmvgdGiM24nMI1KspgD8fH3JD2DEXD8iypuTpp5B',
				'Content-Type: application/json'
		);

		$ch = curl_init(); 
        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url); 
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
		
		$result = curl_exec ( $ch );
		curl_close ( $ch );
		
		return $result;
	}
}
