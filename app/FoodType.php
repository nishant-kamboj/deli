<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FoodType extends Model
{
    public function foodCategories()
	{
		return $this->belongsToMany('App\FoodCategory', 'food_type_items', 'type_id', 'item_id');
	}
}
