<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    public function extraIngredients()
	{
		return $this->belongsToMany('App\FoodIngredient', 'ordered_detail_ingredients', 'detail_id', 'ingredient_id');
	}
}
