<?php
// DELETE MENU SHOULD NOT BE ALLOWED
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FoodType;
use App\FoodItem;
use App\FoodIngredient;
use App\FoodCategory;
use Auth;
class MenusController extends Controller
{
    public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('hotel.named');
	}
	
	public function manageMenus()
	{
		$menus = FoodType::where('status', 1)->where('hotel_id', Auth::user()->hotel_id)->get();
		return view('manage-menus', compact('menus'));
	}
	
	public function editMenu($menu_id)
	{
		$menu = FoodType::find($menu_id);
		if($menu) {
			return view('edit-menu', compact('menu'));
		}
		
		$response['status'] 	= 0;
		$response['message'] 	= 'Menu not found!';
		session()->flash('response', $response);
		return redirect()->route('manage-menus');
		// Try to take if-else response abstract so that I can change it anytime from one place.
	}
	
	public function submitMenu(Request $request)
	{
		$menu = FoodType::where('name', $request->menu_name)->where('id', '!=', $request->menu_id)->where('status', 1)->where('hotel_id', Auth::user()->hotel_id)->first();
		if($menu == null) {
			$menu = FoodType::find($request->menu_id);
			if($menu) {
				$image = $request->image;
				
				if(!is_null($image)) {
					$image_name = 'food_types/'.time().'.'.$image->getClientOriginalExtension();
					$image->move(public_path('food_types'), $image_name);
					$menu->image 		= $image_name;
				}
				
				$menu->name 		= ucfirst($request->menu_name);
				$menu->starts_at 	= $request->starts_at;
				$menu->ends_at 		= $request->ends_at;
				$menu->save();

				$response['status'] 	= 1;
				$response['message'] 	= 'Menu has been successfully updated!';

				session()->flash('response', $response);
				return redirect()->route('edit-menu', $request->menu_id);
			}
			$response['status'] 	= 0;
			$response['message'] 	= 'Menu not found!';
		}else {
			$response['status'] 	= 0;
			$response['message'] 	= 'Name Already Exist. Please try with another Name';
		}
		session()->flash('response', $response);
		return redirect()->route('manage-menus');
	}
	
	public function deleteMenu($menu_id)
	{
		// Make it delete via POST request
		$menu = FoodType::find($menu_id);
		if($menu) {
			$menu->status = 0;
			$menu->save();
			$response['status'] 	= 1;
			$response['message'] 	= $menu->name. ' has been successfully deleted!';
		}
		else {
			$response['status'] 	= 0;
			$response['message'] 	= 'Menu not found!';
		}
		session()->flash('response', $response);
		return redirect()->route('manage-menus');
	}
	
	public function showCreateMenuForm()
	{
		return view('add-food-menu');
	}
	public function createNewMenu(Request $request)
	{
		$response = [];
		$foodType = FoodType::where('name', $request->menu_name)->where('hotel_id', Auth::user()->hotel_id)->where('status', 1)->first();
		if($foodType == null) {
			$file = $request->file('image');
			$destinationPath = public_path()."/food_types/";
			$fileName = time()."_".$file->getClientOriginalName();
			$filePathFromPublicDir = "food_types/".$fileName;
			$file->move($destinationPath,$fileName);
			
			$foodType = new FoodType();
			$foodType->name = ucfirst($request->menu_name);
			$foodType->image = $filePathFromPublicDir;
			$foodType->hotel_id = Auth::user()->hotel_id;
			$foodType->starts_at = $request->starts_at;
			$foodType->ends_at = $request->ends_at;
			$foodType->status = 1;
			$foodType->save();
		
			$response['status'] 	= 1;
			$response['message'] 	= 'Menu Created Successfully.';
		}
		else{
			$response['status'] 	= 0;
			$response['message'] 	= 'Name Already Exist. Please try with another Name';
		}
		session()->flash('response', $response);
		return redirect()->back();
	}
	
	public function showFoodItemForm()
	{
		$food_ingredients = FoodIngredient::where('hotel_id', Auth::user()->hotel_id)->get();
		$foodCategories = FoodCategory::where('status', 1)->where('hotel_id', Auth::user()->hotel_id)->get();
		return view('add-food-item', compact('food_ingredients','foodCategories'));
	}
	
	
	public function manageFoodItems()
	{
		$foodCategoryIds = FoodCategory::where('status', 1)->where('hotel_id', Auth::user()->hotel_id)->pluck('id');
		$food_items = FoodItem::whereIn('category_id', $foodCategoryIds)->get();
		return view('manage-food-items', compact('food_items'));
	}
	
	public function editFoodItem($food_item_id)
	{
		$food_item = FoodItem::find($food_item_id);
		if($food_item) {
			$food_ingredients = FoodIngredient::where('hotel_id', Auth::user()->hotel_id)->get();
			$linked_ingredient_ids = $food_item->foodIngredients->pluck('id')->toArray();
			$foodCategories = FoodCategory::where('status', 1)->where('hotel_id', Auth::user()->hotel_id)->get();
			return view('edit-food-item', compact('food_item', 'food_ingredients', 'linked_ingredient_ids', 'foodCategories'));
		}
		
		$response['status'] 	= 0;
		$response['message'] 	= 'Food Item not found!';
		session()->flash('response', $response);
		return redirect()->route('manage-food-items');
	}
	
	public function submitFoodItem(Request $request)
	{
		$food_item = new FoodItem();
		$image = $request->image;
		
		if(!is_null($image)) {
			$image_name = 'food_items/'.time().'.'.$image->getClientOriginalExtension();
			$image->move(public_path('food_items'), $image_name);
			$food_item->image = $image_name;
		}
		
		$food_item->name 		= $request->item_name;
		$food_item->description = $request->description;
		$food_item->price 		= $request->price;
		$food_item->discount 	= $request->discount;
		//$food_item->veg_status 	= $request->type;
		$food_item->category_id	= $request->food_category_id;
		$food_item->status 		= 1;
		
		if($food_item->save()) {
			$food_item->foodIngredients()->sync($request->food_ingredient_ids);
			$response['status'] 	= 1;
			$response['message'] 	= 'Food Item has been created successfully!';
			session()->flash('response', $response);
			return redirect()->route('manage-food-items');
		}
		else {
			$response['status'] 	= 0;
			$response['message'] 	= 'Something went wrong, please try again later!';
		}
		session()->flash('response', $response);
		return redirect()->route('add-food-item');
	}
	
	public function submitEditFoodItem(Request $request)
	{
		// Make check for if food item exist or not.
		$food_item = FoodItem::find($request->food_item_id);
		$image = $request->image;
		
		if(!is_null($image)) {
			$image_name = 'food_items/'.time().'.'.$image->getClientOriginalExtension();
			$image->move(public_path('food_items'), $image_name);
			$food_item->image = $image_name;
		}
		
		$food_item->name 		= $request->item_name;
		$food_item->description = $request->description;
		$food_item->price 		= $request->price;
		$food_item->discount 	= $request->discount;
		//$food_item->veg_status 	= $request->type;
		$food_item->category_id	= $request->food_category_id;
		
		if($food_item->save()) {
			$food_item->foodIngredients()->sync($request->food_ingredient_ids);
			$response['status'] 	= 1;
			$response['message'] 	= 'Food Item has been updated successfully!';
			session()->flash('response', $response);
			return redirect()->route('manage-food-items');
		}
		else {
			$response['status'] 	= 0;
			$response['message'] 	= 'Something went wrong, please try again later!';
		}
		session()->flash('response', $response);
		return redirect()->route('edit-food-item', $food_item->id);
	}
	
	public function changeFoodItemStatus(Request $request)
	{
		// Check if exist or not condition
		$food_item = FoodItem::find($request->food_item_id);
		$food_item->status = $request->status;
		$food_item->save();
		$response['status'] 	= 1;
		$response['message'] 	= $food_item->name. '\'s status has been updated!';
		session()->flash('response', $response);
		return redirect()->route('manage-food-items');
	}
	
	public function deleteFoodItem($food_item_id)
	{
		$food_item = FoodItem::find($food_item_id);
		if($food_item) {
			$food_item->delete();
		}
		
		$response['status'] 	= 1;
		$response['message'] 	= $food_item->name. ' has been deleted!';
		session()->flash('response', $response);
		return redirect()->route('manage-food-items');
	}
	
	public function linkFoodItems()
	{
		$menus = FoodType::where('status', 1)->where('hotel_id', Auth::user()->hotel_id)->get();
		$food_items = FoodCategory::where('status', 1)->where('hotel_id', Auth::user()->hotel_id)->get();
		return view('link-food-items', compact('menus', 'food_items'));
	}
	
	public function manageCategories()
	{
		$foodCategories = FoodCategory::where('hotel_id', Auth::user()->hotel_id)->get();
		return view('manage-categories', compact('foodCategories'));
	}
	
	public function changeCategoryStatus(Request $request)
	{
		// Check if exist or not condition
		$foodCategory = FoodCategory::find($request->food_category_id);
		$foodCategory->status = $request->status;
		$foodCategory->save();
		$response['status'] 	= 1;
		$response['message'] 	= $foodCategory->name. '\'s status has been updated!';
		session()->flash('response', $response);
		return redirect()->route('manage-categories');
	}
	
	public function deleteFoodCategory($food_category_id)
	{
		$foodCategory = FoodCategory::find($food_category_id);
		if($foodCategory) {
			$foodCategory->delete();
			$response['status'] 	= 1;
			$response['message'] 	= $foodCategory->name. ' has been deleted!';
		}else {
			$response['status'] 	= 0;
			$response['message'] 	= 'Something went wrong . Please try again';
		}
		
		session()->flash('response', $response);
		return redirect()->route('manage-categories');
	}
	
	public function showCreateCategoryForm()
	{
		$menus = FoodType::where('status', 1)->where('hotel_id', Auth::user()->hotel_id)->get();
		return view('create-category', compact('menus'));
	}
	
	public function createNewCategory(Request $request)
	{
		$response = [];
		$category = FoodCategory::where('name', $request->category_name)->where('hotel_id', Auth::user()->hotel_id)->first();
		if($category == null) {
			$category = new FoodCategory();
			$category->name = $request->category_name;
			$category->image = "";
			$category->hotel_id = Auth::user()->hotel_id;
			$category->status = 1;
			$category->save();
			if($request->menu_ids != null){
				$category->foodTypes()->sync($request->menu_ids);
			}
			$response['status'] 	= 1;
			$response['message'] 	= 'Category Created Successfully.';
			session()->flash('response', $response);
			return redirect()->route('manage-categories');
		}
		else{
			$response['status'] 	= 0;
			$response['message'] 	= 'Name Already Exist. Please try with another Name';
		}
		session()->flash('response', $response);
		return redirect()->back();
	}
	public function editFoodCategory($food_category_id)
	{
		$foodCategory = FoodCategory::find($food_category_id);
		if($foodCategory) {
			return view('edit-food-category', compact('foodCategory'));
		}
		$response['status'] 	= 0;
		$response['message'] 	= 'Food Category not found!';
		session()->flash('response', $response);
		return redirect()->route('manage-categories');		
	}
	public function submitEditFoodCategory(Request $request)
	{
		// Make check for if food item exist or not.
		$foodCategory = FoodCategory::where('name', $request->category_name)->where('id', '!=', $request->food_category_id)->where('hotel_id', Auth::user()->hotel_id)->first();
		if($foodCategory == null){
			$foodCategory = FoodCategory::find($request->food_category_id);		
			$foodCategory->name = $request->category_name;
			$foodCategory->save();
			$response['status'] 	= 1;
			$response['message'] 	= $foodCategory->name.' updated successfully!';
			session()->flash('response', $response);
			return redirect()->route('manage-categories');
		}
		else {
			$response['status'] 	= 0;
			$response['message'] 	= 'Name already exist, please try with another name!';
		}
		session()->flash('response', $response);
		return redirect()->route('edit-category', $request->food_category_id);
	}

	
	public function linkFoodItemsSubmit(Request $request)
	{
		// Check for existence everywhere
		$foodCategory = FoodCategory::find($request->food_item_id);
		$menus = $request->menu_ids;
		$foodCategory->foodTypes()->sync($menus);
		
		$response['status'] 	= 1;
		$response['message'] 	= 'Item has been linked to the menu(s)!';
		session()->flash('response', $response);
		return redirect()->route('link-food-items');
	}
	
	public function getLinkedMenus(Request $request)
	{
		$foodCategory = FoodCategory::find($request->food_item_id);
		return $foodCategory->foodTypes->pluck('id');
		// $counter = 0;
		// $data = '';
		
		// foreach($food_item->foodTypes as $food_type)
		// {
			// if($counter > 1) {
				// $data .= ', ' . $food_type->name;
			// }
			// $data .= $food_type->name;
			// $counter++;
		// }
		// if($counter === 1) {
			// $data .= ' (Only)';
		// }
		// if($counter === 0) {
			// $data .= 'Not linked to any menu';
		// }
		// return $data;
	}

	public function showFoodIngredientForm()
	{
		return view('add-ingredient');
	}

	public function submitFoodIngredient(Request $request)
	{
		// Change naming convention to submitFoodIngredient.
		// Unique key to all names where needed in database.
		$food_ingredient = FoodIngredient::where('name', $request->ingredient_name)->where('hotel_id', Auth::user()->hotel_id)->first();
		if($food_ingredient == null){
		$food_ingredient = new FoodIngredient();
		$food_ingredient->name 		= $request->ingredient_name;
		$food_ingredient->hotel_id	= Auth::user()->hotel_id;
		$food_ingredient->status 	= 1;
		$food_ingredient->save();
		
		$response['status'] 	= 1;
		$response['message'] 	= 'New extra has been created successfully!';
		
		session()->flash('response', $response);
		return redirect()->route('manage-food-ingredients');
		}else {
			$response['status'] 	= 0;
			$response['message'] 	= 'Name already exist, please try with another name!';
		}
		session()->flash('response', $response);
		return redirect()->route('add-food-ingredient');
	}

	public function manageFoodIngredients()
	{
		$food_ingredients = FoodIngredient::where('hotel_id', Auth::user()->hotel_id)->get();
		return view('manage-food-ingredients', compact('food_ingredients'));
	}

	public function editFoodIngredient($food_ingredient_id)
	{
		$food_ingredient = FoodIngredient::find($food_ingredient_id);
		if($food_ingredient) {
			return view('edit-food-ingredient', compact('food_ingredient'));
		}
		
		$response['status'] 	= 0;
		$response['message'] 	= 'Food Ingredient not found!';
		session()->flash('response', $response);
		return redirect()->route('manage-food-ingredients');		
	}

	public function submitEditFoodIngredient(Request $request)
	{
		// Make check for if food item exist or not.
		$food_ingredient = FoodIngredient::where('name', $request->ingredient_name)->where('id', '!=', $request->food_ingredient_id)->where('hotel_id', Auth::user()->hotel_id)->first();
		if($food_ingredient == null){
			$food_ingredient = FoodIngredient::find($request->food_ingredient_id);		
			$food_ingredient->name = $request->ingredient_name;
			$food_ingredient->save();
			$response['status'] 	= 1;
			$response['message'] 	= $food_ingredient->name.' updated successfully!';
			session()->flash('response', $response);
			return redirect()->route('manage-food-ingredients', $food_ingredient->id);
		}
		else {
			$response['status'] 	= 0;
			$response['message'] 	= 'Name already exist, please try with another name!';
		}
		session()->flash('response', $response);
		return redirect()->route('edit-food-ingredient', $request->food_ingredient_id);
	}

	public function changeFoodIngredientStatus(Request $request)
	{
		// Check if exist or not condition
		$food_ingredient = FoodIngredient::find($request->food_ingredient_id);
		$food_ingredient->status = $request->status;
		$food_ingredient->save();
		$response['status'] 	= 1;
		$response['message'] 	= $food_ingredient->name. '\'s status has been updated!';
		session()->flash('response', $response);
		return redirect()->route('manage-food-ingredients');
	}
	
	public function deleteFoodIngredient($food_ingredient_id)
	{
		$food_ingredient = FoodIngredient::find($food_ingredient_id);
		if($food_ingredient) {
			$food_ingredient->delete();
		}
		
		$response['status'] 	= 1;
		$response['message'] 	= $food_ingredient->name. ' has been deleted!';
		session()->flash('response', $response);
		return redirect()->route('manage-food-ingredients');
	}

}
