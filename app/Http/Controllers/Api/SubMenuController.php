<?php
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\User;
use App\FoodItem;
use App\FoodType;
use App\FoodCategory;
class SubMenuController extends Controller
{
    
    /**
     * Create subMenu
     *
     * @return [json] FoodItem object
     */
    public function CreateSubMenu(Request $request)
    {
        $response;
        $validator = Validator::make($request->all(),[
            'name' => 'required|string',
            'price' => 'required',
			'category_id' => 'required|integer',
			'image' => 'mimes:jpeg,jpg,png,gif|required|max:30000' // size in kb
        ]);
		if ($validator->fails()) {
			$errorMessage = "";
			$errorArray = json_decode($validator->messages());
			foreach($errorArray as $key => $value) {
				$errorMessage = $errorMessage.$value[0].", ";
			}
			$errorMessage = substr($errorMessage,0,strlen($errorMessage)-2);
			$response = [
							'message' 	=> $errorMessage,
							'code'		=> 500,
							'status' 	=> false
						];
		}else{
			$file = $request->file('image');
			$destinationPath = public_path()."/food_items/";
			$fileName = time()."_".$file->getClientOriginalName();
			$filePathFromPublicDir = "food_items/".$fileName;
			$file->move($destinationPath,$fileName);
			
			$subMenu = new FoodItem();
			$subMenu->name = $request->name;
			$subMenu->description = $request->description != null ? $request->description : "";
			$subMenu->image = $filePathFromPublicDir;
			$subMenu->price = $request->price;
			$subMenu->discount = $request->discount != null ? $request->discount : 0;
			$subMenu->category_id = $request->category_id;
			$subMenu->status = 1;
			$subMenu->save();
			if($request->ingredients != null){
				$subMenu->foodIngredients()->sync(explode(',', $request->ingredients));
			}
			$response = [
							'message' 	=> 'Successfully created SubMenu!',
							'code'		=> 200,
							'status' 	=> true
						];
		}
        return json_encode($response);
    }
    /**
     * Get All subMenus
     *
     * @return [json] FoodItem object
     */
    public function GetSubMenus(Request $request)
    {
        $response;
        $validator = Validator::make($request->all(),[
            'menu_id' => 'required|integer',
        ]);
		if ($validator->fails()) {
			$errorMessage = "";
			$errorArray = json_decode($validator->messages());
			foreach($errorArray as $key => $value) {
				$errorMessage = $errorMessage.$value[0].", ";
			}
			$errorMessage = substr($errorMessage,0,strlen($errorMessage)-2);
			$response = [
							'message' 	=> $errorMessage,
							'code'		=> 500,
							'status' 	=> false,
							'menu_name'	=> "",
							'menu_id'	=> 0,
							'subMenus'	=> array()
						];
		}else{
			$foodType = FoodType::find($request->menu_id);
			$categories;
			$listFoundStatus = false;
			if($request->menu_id != 0) {
				$categories = $foodType->foodCategories->where('status',1)->where('hotel_id', $request->user()->hotel_id);
			}else {
				$categories = FoodCategory::where('status',1)->where('hotel_id', $request->user()->hotel_id)->get();
			}
			//FoodItem::where('status', 1)->get();
			if (!$categories->isEmpty()) {
				$categoriesArray = array();
				foreach($categories as $category){
					$subMenus = $request->user()->role == 2 ? $category->foodItems : $category->foodItems->where('status', 1);
					$subMenusArray = array();
					if(!$subMenus->isEmpty()){
						$listFoundStatus = true;
						foreach($subMenus as $subMenu){
							$discount_price = $subMenu->discount != 0.00 ? $subMenu->price-(($subMenu->price*$subMenu->discount)/100) : $subMenu->price;
							array_push($subMenusArray, array(
								'id' 				=> $subMenu->id,
								'name'				=> $subMenu->name,
								'description'		=> $subMenu->description ?? "",
								'image'				=> asset($subMenu->image),
								'price'				=> "".$subMenu->price,
								'discount'			=> "".$subMenu->discount,
								'discount_price'	=> "".$discount_price,
								'ingredients'		=> $subMenu->foodIngredients,
								'submenu_status'	=> $subMenu->status,
								'category_id'		=> $category->id,
								'category_name'		=> $category->name,
							));
						}
					}
					if(!empty($subMenusArray)){
						array_push($categoriesArray, array(
									//'subMenus' => $subMenusArray,
									//'image'				=> asset($category->image),
									
									////////////////////////////////////////////
									
									 'id'		=> $category->id,
									 'name'		=> $category->name,
									 'subMenus'	=> $subMenusArray
									
								));
					}
				}
			}
			if($listFoundStatus) {
				$response = [
								'message' 	=> 'List Fetched Successfully',
								'code'		=> 200,
								'status' 	=> true,
								'menu_name'	=> $foodType != null ? $foodType->name : "",
								'menu_id'	=> $foodType != null ? $foodType->id : 0,
								'categories'=> $categoriesArray
							];
			}
			else {
				$response = [
									'message' 	=> $foodType != null ? $foodType->name.' Not Found.' : 'List Not Found.',
									'code'		=> 404,
									'status' 	=> false,
									'menu_name'	=> $foodType != null ? $foodType->name : "",
									'menu_id'	=> $foodType != null ? $foodType->id : 0,
									'categories'	=> array()
				];
			}
		}
        return json_encode($response);
    }
	/**
     * change status ( 1 -> active , 0 -> inactive )
     *
     * @return [json] object
     */
    public function PublishUnpublishSubMenu(Request $request)
    {
        $response;
        $validator = Validator::make($request->all(),[
            'submenu_id' => 'required|integer',
            'submenu_status' => 'required|integer',
            'menu_id' => 'required|integer',
        ]);
		if ($validator->fails()) {
			$errorMessage = "";
			$errorArray = json_decode($validator->messages());
			foreach($errorArray as $key => $value) {
				$errorMessage = $errorMessage.$value[0].", ";
			}
			$errorMessage = substr($errorMessage,0,strlen($errorMessage)-2);
			$response = [
							'message' 	=> $errorMessage,
							'code'		=> 500,
							'status' 	=> false
						];
		}else{
			if(FoodItem::where('id', $request->submenu_id)->update(['status' => $request->submenu_status])){
				$foodType = FoodType::find($request->menu_id);
				$categories;
				$listFoundStatus = false;
				if($request->menu_id != 0) {
					$categories = $foodType->foodCategories->where('status',1)->where('hotel_id', $request->user()->hotel_id);
				}else {
					$categories = FoodCategory::where('status',1)->where('hotel_id', $request->user()->hotel_id)->get();
				}
				//FoodItem::where('status', 1)->get();
				if (!$categories->isEmpty()) {
					$categoriesArray = array();
					foreach($categories as $category){
						$subMenus = $category->foodItems;
						$subMenusArray = array();
						if(!$subMenus->isEmpty()){
							$listFoundStatus = true;
							foreach($subMenus as $subMenu){
								$discount_price = $subMenu->discount != 0.00 ? $subMenu->price-(($subMenu->price*$subMenu->discount)/100) : $subMenu->price;
								array_push($subMenusArray, array(
									'id' 				=> $subMenu->id,
									'name'				=> $subMenu->name,
									'description'		=> $subMenu->description ?? "",
									'image'				=> asset($subMenu->image),
									'price'				=> "".$subMenu->price,
									'discount'			=> "".$subMenu->discount,
									'discount_price'	=> "".$discount_price,
									'ingredients'		=> $subMenu->foodIngredients,
									'submenu_status'	=> $subMenu->status,
									'category_id'		=> $category->id,
									'category_name'		=> $category->name,
								));
							}
						}
						if(!empty($subMenusArray)){
							array_push($categoriesArray, array(
										//'subMenus' => $subMenusArray,
										//'image'				=> asset($category->image),
										
										////////////////////////////////////////////
										
										 'id'		=> $category->id,
										 'name'		=> $category->name,
										 'subMenus'	=> $subMenusArray
										
									));
						}
					}
				}
				
				$response = [
								'message' 	=> 'Updated Successfully',
								'code'		=> 200,
								'status' 	=> true,
								'menu_name'	=> $foodType != null ? $foodType->name : "",
								'menu_id'	=> $foodType != null ? $foodType->id : 0,
								'categories'=> $categoriesArray,
							];
			}
			else {
				$response = [
							'message' 	=> 'Sub Menu Not Found',
							'code'		=> 404,
							'status' 	=> false,
						];
			}
		}
        return json_encode($response);
    }
	/**
     * delete submenu
     *
     * @return [json] object
     */
    public function DeleteSubMenu(Request $request)
    {
        $response;
        $validator = Validator::make($request->all(),[
            'submenu_ids' => 'required|string',
        ]);
		if ($validator->fails()) {
			$response = [
							'message' 	=> 'submenu_ids Field is Required',
							'code'		=> 500,
							'status' 	=> false
						];
		}else{
			$ids = explode(",", $request->submenu_ids);
			if(FoodItem::whereIn('id',$ids)->delete()){
				$response = [
								'message' 	=> 'Deleted Successfully',
								'code'		=> 200,
								'status' 	=> true
							];
			}
			else {
				$response = [
							'message' 	=> 'Sub Menu Not Found',
							'code'		=> 404,
							'status' 	=> false,
						];
			}
		}
        return json_encode($response);
    }
	/**
     * Edit SubMenu
     *
     * @return [json] object
     */
    public function EditSubMenu(Request $request)
    {
		$response;
        $validator = Validator::make($request->all(),[
			'submenu_id' => 'required|integer',
            'name' => 'required|string',
            'price' => 'required',
			'image' => 'mimes:jpeg,jpg,png,gif|max:30000' // size in kb
        ]);
		if ($validator->fails()) {
			$errorMessage = "";
			$errorArray = json_decode($validator->messages());
			foreach($errorArray as $key => $value) {
				$errorMessage = $errorMessage.$value[0].", ";
			}
			$errorMessage = substr($errorMessage,0,strlen($errorMessage)-2);
			$response = [
							'message' 	=> $errorMessage,
							'code'		=> 500,
							'status' 	=> false
						];
		}else{
			$file = $request->file('image');
			if(!$file == null){
				$destinationPath = public_path()."/food_items/";
				$fileName = time()."_".$file->getClientOriginalName();
				$filePathFromPublicDir = "food_items/".$fileName;
				$file->move($destinationPath,$fileName);
				
				$subMenu = FoodItem::find($request->submenu_id);
				$subMenu->name = $request->name;
				$subMenu->description = $request->description != null ? $request->description : "";
				$subMenu->image = $filePathFromPublicDir;
				$subMenu->price = $request->price;
				$subMenu->discount = $request->discount != null ? $request->discount : 0;
				$subMenu->save();
				if($request->ingredients != null){
					$subMenu->foodIngredients()->sync(explode(',', $request->ingredients));
				}
			}
			else{
				$subMenu = FoodItem::find($request->submenu_id);
				$subMenu->name = $request->name;
				$subMenu->description = $request->description;
				$subMenu->price = $request->price;
				$subMenu->discount = $request->discount;
				$subMenu->status = 1;
				$subMenu->save();
				if($request->ingredients != null){
					$subMenu->foodIngredients()->sync(explode(',', $request->ingredients));
				}
			}
			$response = [
							'message' 	=> 'SubMenu updated successfully',
							'code'		=> 200,
							'status' 	=> true
						];
		}
        return json_encode($response);
    }
}