<?php
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\User;
use App\UserDeviceInfo;
use App\FirebaseNotification;
class FirebaseController extends Controller
{
    
    /**
     * Send Notification
     *
     * @return [json] user object
     */
    public function SendNotification(Request $request) {
		$userIds = User::where('hotel_id', $request->user()->hotel_id)->where('id','!=' , $request->user()->id)->pluck('id');
		//$deviceTokens = UserDeviceInfo::whereIn('user_id', $userIds)->pluck('device_token');
		$deviceTokens = array("fNYxlbwohs8:APA91bGeTtSx5UM3R21QCC92CFY_s0TOHorg8UUcYvG_g21Y8nhUS-I3FbKtGvNr_oRgR4z4sKY9UO9-dF3HGwS8L3tOBY1Zzdy5x1flH4-UQ3SW6bvCnSMlKjj5iHxJw1H0k_xccoZs");
		$message = "hi";
		$title = "Order Confirmed";
		$result = json_decode(FirebaseNotification::instance()->sendNotification($title, $message, $deviceTokens));
		
		if($result->success){
			echo $result->success;
		}
		return $result;
	}
}