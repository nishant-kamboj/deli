<?php
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\User;
use App\UserDeviceInfo;
use App\Hotel;
class LoginLogoutController extends Controller
{
	/**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function UserLogin(Request $request)
    {
		$response;
		//$request['email'];
		//return response()->json(['email' => $request->email]);
        $validator = Validator::make($request->all(),[
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean',
			'os_type' => 'required|string',
			'os_version' => 'required|string',
			'device_model' => 'required|string',
			'device_token' => 'required|string',
			'app_version' => 'required|string'
        ]);
		
		if ($validator->fails()) {
			$errorMessage = "";
			$errorArray = json_decode($validator->messages());
			foreach($errorArray as $key => $value) {
				$errorMessage = $errorMessage.$value[0].", ";
			}
			$errorMessage = substr($errorMessage,0,strlen($errorMessage)-2);
			$response = [
							'message' 	=> $errorMessage,
							'response_code' => 500,
							'status' => false,
						];
		}else{
			$credentials = ['email' => $request->email, 'password' => $request->password];
			if(!Auth::attempt($credentials)){
				$user = User::where('email', $request->email)->first();
				if($user == null){
					$response = [
						'message' 		=> 'Email not found',
						'response_code'	=> 401,
						'status'		=> false,
						'data'			=> array()
					];
				}
				else{
					$response = [
						'message' 		=> 'Wrong password',
						'response_code'	=> 401,
						'status'		=> false,
						'data'			=> array()
					];
				}
			}else{
				$user = $request->user();
				$userDeviceInfo = UserDeviceInfo::where('user_id', $user->id)->first();
				
				if($userDeviceInfo != null){
					$userDeviceInfo->user_id = $user->id;
					$userDeviceInfo->os_type = $request->os_type;
					$userDeviceInfo->os_version = $request->os_version;
					$userDeviceInfo->device_model = $request->device_model;
					$userDeviceInfo->device_token = $request->device_token;
					$userDeviceInfo->app_version = $request->app_version;
				}else {
					$userDeviceInfo = new UserDeviceInfo();
					$userDeviceInfo->user_id = $user->id;
					$userDeviceInfo->os_type = $request->os_type;
					$userDeviceInfo->os_version = $request->os_version;
					$userDeviceInfo->device_model = $request->device_model;
					$userDeviceInfo->device_token = $request->device_token;
					$userDeviceInfo->app_version = $request->app_version;
				}
				$userDeviceInfo->save();
				
				$tokenResult = $user->createToken('Personal Access Token');
				$token = $tokenResult->token;
				if ($request->remember_me)
					$token->expires_at = Carbon::now()->addWeeks(1);
				$token->save();
				
				$hotel = Hotel::find($user->hotel_id);
				$hotelResponse = array('id'				=> $hotel->id,
									'name'			=> $hotel->name != null ? $hotel->name : "",
									'email' 		=> $hotel->email != null ? $hotel->email : "",
									'contact_no'	=> $hotel->contact_no != null ? $hotel->contact_no : "",
									'address'		=> $hotel->address != null ? $hotel->address : "",
									'image'			=> $hotel->image != null ? asset($hotel->image) : "",
									'hotel_type'	=> $hotel->hotel_type."",
									);
				
				$userResponse = array('id'		=> $user->id,
									'name'		=> $user->name,
									'email'		=> $user->email,
									'mobile_no'	=> $user->mobile_no != null ? $user->mobile_no : "",
									'role'		=> $user->role,
									'hotel'		=> $hotelResponse
									);
				$response = [
					'access_token'	=> $tokenResult->accessToken,
					'message' 		=> 'Login Successful',
					'response_code' => 200,
					'status' 		=> true,
					'first_login'	=> $hotel->name != null ? 1 : 0,
					'user' 			=> $userResponse
					
				];
			}
		}
		return json_encode($response);
    }
	
	/**
     * Login device and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function DeviceLogin(Request $request)
    {
		$response;
		//$request['email'];
		//return response()->json(['email' => $request->email]);
        $validator = Validator::make($request->all(),[
            'email' => 'required|string',
            'password' => 'required|string',
            'remember_me' => 'boolean',
			'os_type' => 'required|string',
			'os_version' => 'required|string',
			'device_model' => 'required|string',
			'device_token' => 'required|string',
			'app_version' => 'required|string'
        ]);
		
		if ($validator->fails()) {
			$errorMessage = "";
			$errorArray = json_decode($validator->messages());
			foreach($errorArray as $key => $value) {
				$errorMessage = $errorMessage.$value[0].", ";
			}
			$errorMessage = substr($errorMessage,0,strlen($errorMessage)-2);
			$response = [
							'message' 	=> $errorMessage,
							'code'	=> 500,
							'status' 		=> false,
						];
		}else{
			$credentials = ['email' => $request->email, 'password' => $request->password];
			if(!Auth::attempt($credentials)){
				$user = User::where('email', $request->email)->first();
				if($user == null){
					$response = [
						'message' 		=> 'Email not found',
						'code'			=> 401,
						'status'		=> false,
					];
				}
				else{
					$response = [
						'message' 		=> 'Wrong password',
						'code'			=> 401,
						'status'		=> false,
					];
				}
			}else{
				$user = $request->user();
				$userDeviceInfo = UserDeviceInfo::where('user_id', $user->id)->first();
				
				if($userDeviceInfo != null){
					$userDeviceInfo->user_id = $user->id;
					$userDeviceInfo->os_type = $request->os_type;
					$userDeviceInfo->os_version = $request->os_version;
					$userDeviceInfo->device_model = $request->device_model;
					$userDeviceInfo->device_token = $request->device_token;
					$userDeviceInfo->app_version = $request->app_version;
				}else {
					$userDeviceInfo = new UserDeviceInfo();
					$userDeviceInfo->user_id = $user->id;
					$userDeviceInfo->os_type = $request->os_type;
					$userDeviceInfo->os_version = $request->os_version;
					$userDeviceInfo->device_model = $request->device_model;
					$userDeviceInfo->device_token = $request->device_token;
					$userDeviceInfo->app_version = $request->app_version;
				}
				$userDeviceInfo->save();
				
				$tokenResult = $user->createToken('Personal Access Token');
				$token = $tokenResult->token;
				if ($request->remember_me)
					$token->expires_at = Carbon::now()->addWeeks(1);
				$token->save();
				
				$hotel = Hotel::find($user->hotel_id);
				$hotelResponse = array('id'				=> $hotel->id,
									'name'			=> $hotel->name != null ? $hotel->name : "",
									'email' 		=> $hotel->email != null ? $hotel->email : "",
									'contact_no'	=> $hotel->contact_no != null ? $hotel->contact_no : "",
									'address'		=> $hotel->address != null ? $hotel->address : "",
									'image'			=> $hotel->image != null ? asset($hotel->image) : "",
									'hotel_type'	=> $hotel->hotel_type."",
									);
				
				$userResponse = array('id'		=> $user->id,
									'name'		=> $user->name,
									'email'		=> $user->email,
									'hotel_id'	=> $user->hotel_id,
									'mobile_no'	=> $user->mobile_no != null ? $user->mobile_no : "",
									'email_verified_at'	=> $user->email_verified_at != null ? $user->email_verified_at : "",
									'role'		=> $user->role,
									'created_at'=> $user->created_at->format('Y-m-d h:m:s'),
									'updated_at'=> $user->updated_at->format('Y-m-d h:m:s'),
									'hotel'		=> $hotelResponse,
									);
									
				$response = [
						
					'access_token'	=> $tokenResult->accessToken,
					'message' 		=> 'Login Successful',
					'code' 			=> 200,
					'status' 		=> true,
					'user' 			=> $userResponse
				];
			}
		}
		return json_encode($response);
    }
	 
	/**
	* Logout user (Revoke the token)
	*
	* @return [string] message
	*/
    public function Logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out',
			'code' 			=> 200,
			'status' 		=> true,
        ]);
    }
}