<?php
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\User;
use App\FoodType;
use App\FoodCategory;
class CategoryController extends Controller
{
    
    /**
     * Create Category
     *
     * @return [json] FoodItem object
     */
    public function CreateCategory(Request $request)
    {
        $response;
        $validator = Validator::make($request->all(),[
            'name' => 'required|string|unique:food_categories',
			'menu_id' => 'required|integer',
			//'image' => 'mimes:jpeg,jpg,png,gif|required|max:30000' // size in kb
        ]);
		if ($validator->fails()) {
			$errorMessage = "";
			$errorArray = json_decode($validator->messages());
			foreach($errorArray as $key => $value) {
				$errorMessage = $errorMessage.$value[0].", ";
			}
			$errorMessage = substr($errorMessage,0,strlen($errorMessage)-2);
			$response = [
							'message' 	=> $errorMessage,
							'code'		=> 500,
							'status' 	=> false
						];
		}else{
			
			$category = new FoodCategory();
			$category->name = $request->name;
			$category->hotel_id = $request->user()->hotel_id;
			$category->image = "";
			$category->save();
			
			$foodType = FoodType::find($request->menu_id);
			$foodType->foodCategories()->attach($category->id);
			$response = [
							'message' 	=> 'Category Successfuly Created !',
							'code'		=> 200,
							'status' 	=> true
						];
		}
        return json_encode($response);
    }
	
	/**
     * Get All Categories
     *
     * @return [json] FoodItem object
     */
    public function GetCategories(Request $request)
    {
        $response;
        $validator = Validator::make($request->all(),[
            'menu_id' => 'required|integer',
        ]);
		if ($validator->fails()) {
			$errorMessage = "";
			$errorArray = json_decode($validator->messages());
			foreach($errorArray as $key => $value) {
				$errorMessage = $errorMessage.$value[0].", ";
			}
			$errorMessage = substr($errorMessage,0,strlen($errorMessage)-2);
			$response = [
							'message' 	=> $errorMessage,
							'code'		=> 500,
							'status' 	=> false,
							'categories'=> array()
						];
		}else{
			$foodTypes = $request->menu_id == 0 ? FoodType::where('hotel_id',$request->user()->hotel_id)->get() : FoodType::where('id',$request->menu_id)->get();
			$responseFound = false;
			$categoriesArray = array();
			foreach($foodTypes as $foodType) {
				$categories = $request->menu_id == 0 ? $foodType->foodCategories : $foodType->foodCategories->where('status',1);
				//FoodItem::where('status', 1)->get();
				if (!$categories->isEmpty()) {
					$responseFound = true;
					foreach($categories as $category){
						array_push($categoriesArray, array(
									'id'				=> $category->id,
									'name'				=> $category->name,
									'category_status'	=> $category->status."",
									'menu_name'			=> $foodType->name,
									'menu_id'			=> $foodType->id,
								));
					}
				}
			}
			if($responseFound){
				$response = [
								'message' 	=> "List Fetched Successfully",
								'code'		=> 200,
								'status' 	=> true,
								'categories'=> $categoriesArray
							];
			}else{
				$response = [
								'message' 	=> "Category Not Found.",
								'code'		=> 404,
								'status' 	=> false,
								'categories'=> $categoriesArray
							];
			}
		}
        return json_encode($response);
    }
	/**
     * change status ( 1 -> active , 0 -> inactive )
     *
     * @return [json] object
     */
    public function publishUnpublishCategory(Request $request)
    {
        $response;
        $validator = Validator::make($request->all(),[
            'category_id' 		=> 'required|integer',
			'category_status'	=> 'required|integer',	
        ]);
		if ($validator->fails()) {
			$errorMessage = "";
			$errorArray = json_decode($validator->messages());
			foreach($errorArray as $key => $value) {
				$errorMessage = $errorMessage.$value[0].", ";
			}
			$errorMessage = substr($errorMessage,0,strlen($errorMessage)-2);
			$response = [
							'message' 	=> $errorMessage,
							'code'		=> 500,
							'status' 	=> false
						];
		}else{
			if(FoodCategory::where('id', $request->category_id)->update(['status' => $request->category_status])){
				$foodTypes = FoodType::where('hotel_id', $request->user()->hotel_id)->get();
				$responseFound = false;
				$categoriesArray = array();
				foreach($foodTypes as $foodType) {
					$categories = $foodType->foodCategories;
					//FoodItem::where('status', 1)->get();
					if (!$categories->isEmpty()) {
						$responseFound = true;
						foreach($categories as $category){
							array_push($categoriesArray, array(
										'id'				=> $category->id,
										'name'				=> $category->name,
										'category_status'	=> $category->status."",
										'menu_name'			=> $foodType->name,
										'menu_id'			=> $foodType->id,
									));
						}
					}
				}
				$response = [
								'message' 	=> 'Updated Successfully',
								'code'		=> 200,
								'status' 	=> true,
								'categories'=> $categoriesArray
							];
			}
			else {
				$response = [
							'message' 	=> 'Category Not Found',
							'code'		=> 404,
							'status' 	=> false,
						];
			}
		}
        return json_encode($response);
    }
	/**
     * delete Categories )
     *
     * @return [json] object
     */
    public function DeleteCategory(Request $request)
    {
        $response;
        $validator = Validator::make($request->all(),[
            'category_ids' => 'required|string',
        ]);
		if ($validator->fails()) {
			$errorMessage = "";
			$errorArray = json_decode($validator->messages());
			foreach($errorArray as $key => $value) {
				$errorMessage = $errorMessage.$value[0].", ";
			}
			$errorMessage = substr($errorMessage,0,strlen($errorMessage)-2);
			$response = [
							'message' 	=> $errorMessage,
							'code'		=> 500,
							'status' 	=> false
						];
		}else{
			$ids = explode(",", $request->category_ids);
			if(FoodCategory::whereIn('id',$ids)->delete()){
				$response = [
								'message' 	=> 'Deleted Successfully',
								'code'		=> 200,
								'status' 	=> true
							];
			}
			else {
				$response = [
							'message' 	=> 'Category Not Found',
							'code'		=> 404,
							'status' 	=> false,
						];
			}
		}
        return json_encode($response);
    }
	/**
     * Edit Category
     *
     * @return [json] object
     */
    public function EditCategory(Request $request)
    {
		$response;
        $validator = Validator::make($request->all(),[
			'category_id' => 'required|integer',
			'name' => 'required|string',
			'menu_id' => 'required',
        ]);
		if ($validator->fails()) {
			$errorMessage = "";
			$errorArray = json_decode($validator->messages());
			foreach($errorArray as $key => $value) {
				$errorMessage = $errorMessage.$value[0].", ";
			}
			$errorMessage = substr($errorMessage,0,strlen($errorMessage)-2);
			$response = [
							'message' 	=> $errorMessage,
							'code'		=> 500,
							'status' 	=> false
						];
		}else{
			$category = FoodCategory::find($request->category_id);
			if($category != null) {
				$category->name = $request->name;
				$category->save();
				$category->foodTypes()->sync(explode(',', $request->menu_id));
				$response = [
							'message' 	=> 'Category updated successfully',
							'code'		=> 200,
							'status' 	=> true
						];
			}
			else{
				$response = [
							'message' 	=> 'Category Not Found',
							'code'		=> 404,
							'status' 	=> true
						];
			}
			
		}
        return json_encode($response);
    }
}