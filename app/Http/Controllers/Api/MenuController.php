<?php
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\User;
use App\FoodType;
use App\Hotel;
use App\FoodCategory;
class MenuController extends Controller
{
	/**
     * Create Menu
     *
     * @return [json] FoodItem object
     */
    public function CreateMenu(Request $request)
    {
		$response;
        $validator = Validator::make($request->all(),[
            'name' => 'required|string',
            'starts_at' => 'required|string',
            'ends_at' => 'required|string',
			'image' => 'mimes:jpeg,jpg,png,gif|required|max:30000' // size in kb
        ]);
		if ($validator->fails()) {
			$errorMessage = "";
			$errorArray = json_decode($validator->messages());
			foreach($errorArray as $key => $value) {
				$errorMessage = $errorMessage.$value[0].", ";
			}
			$errorMessage = substr($errorMessage,0,strlen($errorMessage)-2);
			$response = [
							'message' 	=> $errorMessage,
							'code'		=> 500,
							'status' 	=> false
						];
		}else{
			$foodType = FoodType::where('name', $request->name)->where('hotel_id', $request->user()->hotel_id)->first();
			if($foodType == null) {
				$file = $request->file('image');
				$destinationPath = public_path()."/food_types/";
				$fileName = time()."_".$file->getClientOriginalName();
				$filePathFromPublicDir = "food_types/".$fileName;
				$file->move($destinationPath,$fileName);
				
				$menu = new FoodType();
				$menu->name = $request->name;
				$menu->starts_at = $request->starts_at;
				$menu->ends_at = $request->ends_at;
				$menu->image = $filePathFromPublicDir;
				$menu->hotel_id = $request->user()->hotel_id;
				$menu->save();
				$response = [
								'message' 	=> 'Successfully created Menu!',
								'code'		=> 200,
								'status' 	=> true
							];
			}else {
				$response = [
								'message' 	=> 'Name already exist! Please try with another name',
								'code'		=> 500,
								'status' 	=> true
							];
			}
		}
        return json_encode($response);
    }
	/**
     * Get All Menus
     *
     * @return [json] FoodItem object
     */
    public function GetMenus(Request $request)
    {
        $response;
		$foodTypes = FoodType::where('status', 1)->where('hotel_id', $request->user()->hotel_id)->get();
		if ($foodTypes->isEmpty()) {
			$response = [
							'message' 	=> 'Menu Not Found.',
							'code'		=> 404,
							'status' 	=> false,
							'data'		=> array()
						];
		}else{
			$menus = array();
			foreach($foodTypes as $foodType){
				$totalItems = 0;
				$foodCategories = $foodType->foodCategories()->where('status',1)->get(); 
				foreach($foodCategories as $foodCategory) {
					$totalItems += $foodCategory->foodItems()->where('status',1)->count();
				}
				array_push($menus, array('id'			=> $foodType->id,
										'name'			=> $foodType->name,
										'image'			=> asset($foodType->image),
										'starts_at'		=> $foodType->starts_at,
										'ends_at'		=> $foodType->ends_at,
										'total_items'	=> $totalItems,
				));
			}
			$hotel = Hotel::find($request->user()->hotel_id);
			$hotelResponse = array('id'				=> $hotel->id,
									'name'			=> $hotel->name != null ? $hotel->name : "",
									'email' 		=> $hotel->email != null ? $hotel->email : "",
									'contact_no'	=> $hotel->contact_no != null ? $hotel->contact_no : "",
									'address'		=> $hotel->address != null ? $hotel->address : "",
									'image'			=> $hotel->image != null ? asset($hotel->image) : ""
									);
			$response = [
							'message' 	=> 'List Fetched Successfully',
							'code'		=> 200,
							'status' 	=> true,
							'menus'		=> $menus,
							'hotel'		=> $hotelResponse
						];
		}
        return json_encode($response);
    }
	/**
     * Edit Menu
     *
     * @return [json] object
     */
    public function EditMenu(Request $request)
    {
		$response;
        $validator = Validator::make($request->all(),[
			'menu_id' => 'required|integer',
            'name' => 'required|string',
            'starts_at' => 'required|string',
            'ends_at' => 'required|string',
			'image' => 'mimes:jpeg,jpg,png,gif|max:30000' // size in kb
        ]);
		if ($validator->fails()) {
			$errorMessage = "";
			$errorArray = json_decode($validator->messages());
			foreach($errorArray as $key => $value) {
				$errorMessage = $errorMessage.$value[0].", ";
			}
			$errorMessage = substr($errorMessage,0,strlen($errorMessage)-2);
			$response = [
							'message' 	=> $errorMessage,
							'code'		=> 500,
							'status' 	=> false
						];
		}else{
			$foodType = FoodType::where('name', $request->name)->where('hotel_id', $request->user()->hotel_id)->where('id', '!=', $request->menu_id)->where('status', 1)->first();
			if($foodType == null) {
				$file = $request->file('image');
				if(!$file == null){
					$destinationPath = public_path()."/food_types/";
					$fileName = time()."_".$file->getClientOriginalName();
					$filePathFromPublicDir = "food_types/".$fileName;
					$file->move($destinationPath,$fileName);
					
					$menu = FoodType::find($request->menu_id);
					$menu->name = $request->name;
					$menu->starts_at = $request->starts_at;
					$menu->ends_at = $request->ends_at;
					$menu->image = $filePathFromPublicDir;
					$menu->save();
				}
				else{
					$menu = FoodType::find($request->menu_id);
					$menu->name = $request->name;
					$menu->starts_at = $request->starts_at;
					$menu->ends_at = $request->ends_at;
					$menu->save();
				}
				$response = [
								'message' 	=> 'Menu updated successfully',
								'code'		=> 200,
								'status' 	=> true
							];
			}else {
				$response = [
								'message' 	=> 'Name already exist! Please try with another name',
								'code'		=> 500,
								'status' 	=> true
							];
			}
		}
        return json_encode($response);
    }
	
	/**
     * change status ( 1 -> active , 0 -> inactive )
     *
     * @return [json] object
     */
    public function DeleteMenu(Request $request)
    {
        $response;
        $validator = Validator::make($request->all(),[
            'menu_ids' => 'required|string',
        ]);
		if ($validator->fails()) {
			$errorMessage = "";
			$errorArray = json_decode($validator->messages());
			foreach($errorArray as $key => $value) {
				$errorMessage = $errorMessage.$value[0].", ";
			}
			$errorMessage = substr($errorMessage,0,strlen($errorMessage)-2);
			$response = [
							'message' 	=> $errorMessage,
							'code'		=> 500,
							'status' 	=> false
						];
		}else{
			$ids = explode(",", $request->menu_ids);
			if(FoodType::whereIn('id',$ids)->update(['status' => 0])){
				$response = [
								'message' 	=> 'Deleted Successfully',
								'code'		=> 200,
								'status' 	=> true
							];
			}
			else {
				$response = [
							'message' 	=> 'Menu Not Found',
							'code'		=> 404,
							'status' 	=> false,
							'data'		=> array()
						];
			}
		}
        return json_encode($response);
    }
    
}