<?php
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\User;
use App\FoodIngredient;
use App\FoodItem;
class IngredientController extends Controller
{
    
    /**
     * create Ingredient
     *
     * @return [json] object
     */
    public function CreateIngredient(Request $request)
    {
        $response;
        $validator = Validator::make($request->all(),[
            'name' => 'required|string',
        ]);
		if ($validator->fails()) {
			$errorMessage = "";
			$errorArray = json_decode($validator->messages());
			foreach($errorArray as $key => $value) {
				$errorMessage = $errorMessage.$value[0].", ";
			}
			$errorMessage = substr($errorMessage,0,strlen($errorMessage)-2);
			$response = [
							'message' 	=> $errorMessage,
							'code'		=> 500,
							'status' 	=> false,
							'data'		=> array('exception' => $validator->messages())
						];
		}else{
			$ingredient = FoodIngredient::where('name', $request->name)->where('hotel_id', $request->user()->hotel_id)->first();
			if($ingredient == null){
				$ingredient = new FoodIngredient();
				$ingredient->name = $request->name;
				$ingredient->hotel_id = $request->user()->hotel_id;
				$ingredient->status = 1;
				$ingredient->save();
				$response = [
								'message' 	=> 'Successfully created Ingredient!',
								'code'		=> 200,
								'status' 	=> true,
								'ingredient'=> $ingredient
							];
			}else{
				$response = [
								'message' 	=> 'Name already exist. Please try with another name',
								'code'		=> 500,
								'status' 	=> false,
							];
			}
		}
        return json_encode($response);
    }
	/**
     * get Ingredient
     *
     * @return [json] object
     */
    public function GetIngredients(Request $request)
    {
        $response;
        $validator = Validator::make($request->all(),[
            'submenu_id' => 'required|integer',
        ]);
		if ($validator->fails()) {
			$errorMessage = "";
			$errorArray = json_decode($validator->messages());
			foreach($errorArray as $key => $value) {
				$errorMessage = $errorMessage.$value[0].", ";
			}
			$errorMessage = substr($errorMessage,0,strlen($errorMessage)-2);
			$response = [
							'message' 	=> $errorMessage,
							'code'		=> 500,
							'status' 	=> false,
							'data'		=> array('exception' => $validator->messages())
						];
		}else{
			if($request->submenu_id == 0){
				$ingredients = FoodIngredient::where('status', 1)->where('hotel_id', $request->user()->hotel_id)->get();
				if ($ingredients->isEmpty()) {
					$response = [
									'message' 	=> 'List Not Found',
									'code'		=> 404,
									'status' 	=> false,
									'data'		=> array()
								];
				}else{
					$ingredientsArray = array();
					foreach($ingredients as $ingredient){
						array_push($ingredientsArray, array(
							'id' 		=> $ingredient->id,
							'name'		=> $ingredient->name,
							'status'	=> $ingredient->status,
							'checked'	=> 0
						));
					}
					$response = [
									'message' 	=> 'List Fetched Successfully',
									'code'		=> 200,
									'status' 	=> true,
									'ingredients'=> $ingredientsArray
								];
				}
			}else{
				
				$selectedIngredients = FoodItem::find($request->submenu_id)->foodIngredients;
				$ingredients = FoodIngredient::where('status', 1)->where('hotel_id', $request->user()->hotel_id)->get();
				if ($ingredients->isEmpty()) {
					$response = [
									'message' 	=> 'List Not Found',
									'code'		=> 404,
									'status' 	=> false,
									'data'		=> array()
								];
				}else{
					$ingredientsArray = array();
					foreach($ingredients as $ingredient){
						$selected = 0;
						foreach($selectedIngredients as $selectedIngredient){
							if($ingredient->id == $selectedIngredient->id){
								$selected = 1;
							}
						}
						array_push($ingredientsArray, array(
							'id' 		=> $ingredient->id,
							'name'		=> $ingredient->name,
							'status'	=> $ingredient->status,
							'checked'	=> $selected
						));
					}
					$response = [
									'message' 	=> 'List Fetched Successfully',
									'code'		=> 200,
									'status' 	=> true,
									'ingredients'=> $ingredientsArray
								];
				}
			}
		}
        return json_encode($response);
    }
	/**
     * change status ( 1 -> active , 0 -> inactive )
     *
     * @return [json] object
     */
    public function PublishUnpublishIngredient(Request $request)
    {
        $response;
        $validator = Validator::make($request->all(),[
            'ingredient_id' => 'required|integer',
        ]);
		if ($validator->fails()) {
			$errorMessage = "";
			$errorArray = json_decode($validator->messages());
			foreach($errorArray as $key => $value) {
				$errorMessage = $errorMessage.$value[0].", ";
			}
			$errorMessage = substr($errorMessage,0,strlen($errorMessage)-2);
			$response = [
							'message' 	=> $errorMessage,
							'code'		=> 500,
							'status' 	=> false,
							'data'		=> array('exception' => $validator->messages())
						];
		}else{
			$ingredient = FoodIngredient::where('id', $request->ingredient_id)->first();
			if($ingredient != null){
			$ingredient->status = 0;
			$ingredient->save();
			$response = [
							'message' 	=> 'Deleted Successfully',
							'code'		=> 200,
							'status' 	=> true
						];
			}else {
				$response = [
							'message' 	=> 'Ingredient Not Found',
							'code'		=> 404,
							'status' 	=> false
						];
			}
		}
        return json_encode($response);
    }
	/**
     * delete Ingredient
     *
     * @return [json] object
     */
    public function DeleteIngredient(Request $request)
    {
        $response;
        $validator = Validator::make($request->all(),[
            'ingredient_id' => 'required|integer',
        ]);
		if ($validator->fails()) {
			$errorMessage = "";
			$errorArray = json_decode($validator->messages());
			foreach($errorArray as $key => $value) {
				$errorMessage = $errorMessage.$value[0].", ";
			}
			$errorMessage = substr($errorMessage,0,strlen($errorMessage)-2);
			$response = [
							'message' 	=> $errorMessage,
							'code'		=> 500,
							'status' 	=> false,
							'data'		=> array('exception' => $validator->messages())
						];
		}else{
			if(FoodIngredient::where('id', $request->ingredient_id)->delete()){
			$response = [
							'message' 	=> 'Deleted Successfully',
							'code'		=> 200,
							'status' 	=> true
						];
			}else {
				$response = [
							'message' 	=> 'Ingredient Not Found',
							'code'		=> 404,
							'status' 	=> false
						];
			}
		}
        return json_encode($response);
    }
}