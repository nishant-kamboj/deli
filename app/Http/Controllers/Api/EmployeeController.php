<?php
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\User;
use App\Employee;
use App\OrderDetail;
use App\OrderSummary;
use App\Api\Models\GetEmployees;
use App\Api\Models\EmployeePerformance;
class EmployeeController extends Controller
{
    /**
     * Get All Employees
     *
     * @return [json]
     */
    public function getEmployees(Request $request)
    {
		$getEmployees = new GetEmployees();
		$employees = Employee::where('delete_status', 0)->where('hotel_id', $request->user()->hotel_id)->get();
		if(!$employees->isEmpty()){
			foreach($employees as $employee){
				$getEmployees->addEmployeeToList($employee->id, $employee->name, $employee->email, $employee->mobile_no , $employee->gender, $employee->hotel_id);
			}
			$getEmployees->setMessage("List Fetched Successfully");
			$getEmployees->setResponseCode(200);
			$getEmployees->setStatus(true);
		}
        return $getEmployees->getJson();
    }
	/**
     * Employee Performance
     *
     * @return [json] 
     */
    public function employeePerformance(Request $request)
    {
		$employeePerformance = new EmployeePerformance();
		$validator = Validator::make($request->all(),[
            'from_date' => 'required|string',
            'to_date' => 'required|string',
        ]);
		if ($validator->fails()) {
			$errorMessage = "";
			$errorArray = json_decode($validator->messages());
			foreach($errorArray as $key => $value) {
				$errorMessage = $errorMessage.$value[0].", ";
			}
			$errorMessage = substr($errorMessage,0,strlen($errorMessage)-2);
			$employeePerformance->setMessage($errorMessage);
			$employeePerformance->setResponseCode(500);
			$employeePerformance->setStatus(false);
		}else{
			$fromDate = Carbon::parse($request->from_date);
			$toDate = Carbon::parse($request->to_date);
			$toDate = $toDate->modify('+1 day');
			$employees = Employee::where('delete_status', 0)->where('hotel_id', $request->user()->hotel_id)->orderBy('name')->get();
			if(!$employees->isEmpty()){
				foreach($employees as $employee){
					$totalTakenOrders = 0;
					$totalSpentTime = "00:00:00";
					$orderSummaries = OrderSummary::where('hotel_id', $request->user()->hotel_id)->whereBetween('updated_at',[$fromDate, $toDate])->get();
					foreach($orderSummaries as $orderSummary){
						$orderDetail = $orderSummary->orderDetails->where('prepared_by', $employee->id)->first();
						if($orderDetail != null){
							$startTime = Carbon::parse($orderDetail->prepare_start_time);
							$endTime = Carbon::parse($orderDetail->prepare_end_time);
							$preparationTime = $startTime->diffInSeconds($endTime);
							
							$totalSpentTime = date("H:i:s",strtotime($totalSpentTime) + $preparationTime);
							
							$totalTakenOrders++;
						}
						
					}
					$totalOrders = OrderDetail::where('prepared_by', $employee->id)->count();
					$employeePerformance->addEmployeePerformanceToList($employee->id, $employee->name, $totalTakenOrders, $totalSpentTime);
				}
				$employeePerformance->setMessage("List Fetched Successfully");
				$employeePerformance->setResponseCode(200);
				$employeePerformance->setStatus(true);
			}
		}
        return $employeePerformance->getJson();
    }
}