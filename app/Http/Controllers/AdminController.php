<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\User;
use App\Hotel;
use App\FoodType;
use App\FoodItem;
use App\OrderDetail;
use App\OrderSummary;
use App\Employee;
use Auth;

class AdminController extends Controller
{
    public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('hotel.named')->except(['showHotelDetailsForm', 'submitHotelDetails', 'showAddNewOwnerForm', 'submitAddNewOwner']);
	}
	
	public function showSelectHotel()
	{
		$hotels = Hotel::all();
		return view('select-hotels', compact('hotels'));
	}
	public function submitSelectHotel($hotel_id)
	{
		Auth::user()->hotel_id = $hotel_id;
		Auth::user()->save();
		return redirect()->route('edit-hotel-details');
	}
	public function viewOwners()
	{
		$owners = User::where('hotel_id', Auth::user()->hotel_id)->where('id', '!=', Auth::user()->id)->where('role', 2)->get();
		return view('view-owners', compact('owners'));
	}
	public function editOwner($owner_id)
	{
		$response = [];
		$owner = User::find($owner_id);
		if($owner){
			return view('edit-owner', compact('owner'));
		}
		
		$response['status'] 	= 0;
		$response['message'] 	= 'Device not found!';
		session()->flash('response', $response);
		return redirect()->route('view-owners');
	}
	
	public function submitEditOwner(Request $request)
	{
		$response = [];
		$owner = User::where('email', $request->email)->where('id', '!=', $request->owner_id)->first();
		if($owner == null) {
			$owner = User::where('mobile_no', $request->mobile_no)->where('id', '!=', $request->owner_id)->first();
			if($owner == null) {
				$owner = User::find($request->owner_id);
				$owner->name = $request->name;
				$owner->email = $request->email;
				$owner->mobile_no = $request->mobile_no;
				$owner->save();
			
				$response['status'] 	= 1;
				$response['message'] 	= 'Owner Updated Successfully.';
				
				session()->flash('response', $response);
				return redirect()->route('view-owners');
			}else {
				$response['status'] 	= 0;
				$response['message'] 	= 'Mobile No Already Exist. Please try with another Mobile NO';
			}
		}else {
			$response['status'] 	= 0;
			$response['message'] 	= 'Email Already Exist. Please try with another Email';
		}
		session()->flash('response', $response);
		return redirect()->back();
	}
	public function showAddNewOwnerForm()
	{
		return view('add-new-owner');
	}
	public function submitAddNewOwner(Request $request)
	{
		$response = [];
		$owner = User::where('email', $request->email)->first();
		if($owner == null) {
			$owner = User::where('mobile_no', $request->mobile_no)->first();
			if($owner == null) {
				$hotel = new hotel();
				$hotel->save();
				$owner = new User();
				$owner->name = $request->name;
				$owner->email = $request->email;
				$owner->mobile_no = $request->mobile_no;
				$owner->hotel_id = $hotel->id;
				$owner->password = bcrypt($request->password);
				$owner->role = 2;
				$owner->save();
				
				Auth::user()->hotel_id = $hotel->id;
				Auth::user()->save();
			
				$response['status'] 	= 1;
				$response['message'] 	= 'Owner Created Successfully.';
				
				session()->flash('response', $response);
				return redirect()->route('edit-hotel-details');
			}else {
				$response['status'] 	= 0;
				$response['message'] 	= 'Mobile No Already Exist. Please try with another Mobile NO';
			}
		}else {
			$response['status'] 	= 0;
			$response['message'] 	= 'Email Already Exist. Please try with another Email';
		}
		session()->flash('response', $response);
		return redirect()->back();
	}
	
	public function showHotelDetails()
	{
		$hotels = Hotel::where('id', Auth::user()->hotel_id)->get();
		return view('hotel-details', compact('hotels'));
	}
	
	public function showHotelDetailsForm()
	{
		$hotel = Hotel::find(Auth::user()->hotel_id);
		// if($hotel) {
			return view('edit-hotel-details', compact('hotel'));
		// }

		// session()->flash('response', $response);
		// $response['status'] 	= 0;
		// $response['message'] 	= 'Sorry, You are not registered with any hotel.';
		// return redirect()->back();
	}
	public function submitHotelDetails(Request $request)
	{
		$response = [];
		$updateSuccess = false;
		$hotel = Hotel::where('email', $request->email)->where('id', '!=', Auth::user()->hotel_id)->first();
		if($hotel == null){
			$hotel = Hotel::where('contact_no', $request->contact_no)->where('id', '!=', Auth::user()->hotel_id)->first();
			if($hotel == null){
				$hotel = Hotel::find(Auth::user()->hotel_id);
			
				$file = $request->file('image');
				$hotelTypeDisable = true;
				if($hotel->name == null && $hotel->email == null && $hotel->contact_no == null){
					$hotelTypeDisable = false;
				}
				if(!$file == null){
					$destinationPath = public_path()."/hotels/";
					$fileName = time()."_".$file->getClientOriginalName();
					$filePathFromPublicDir = "hotels/".$fileName;
					$file->move($destinationPath,$fileName);
					
					$hotel->name = $request->hotel_name;
					$hotel->email = $request->email;
					$hotel->contact_no = $request->contact_no;
					$hotel->address = $request->address;
					$hotel->image = $filePathFromPublicDir;
					// hotel type 1 for delio, 0 for pizzario
					$hotel->hotel_type = 1;
					$hotel->save();
				}else {
					$hotel->name 			= $request->hotel_name;
					$hotel->email 			= $request->email;
					$hotel->contact_no 		= $request->contact_no;
					$hotel->address 		= $request->address;
					// hotel type 1 for delio, 0 for pizzario
					$hotel->hotel_type 		= 1;
					$hotel->save();
				}
				
				// only for for pizzario
				/*if(!$hotelTypeDisable){
					$menu = new FoodType();
					$menu->name = "Pizza";
					$menu->starts_at = "00:00:01";
					$menu->ends_at = "23:59:59";
					$menu->image = "food_types/pizza.jpg";
					$menu->hotel_id = $hotel->id;
					$menu->save();
				}*/
				
				$updateMessage = "Hotel Details has been successfully updated";
				$updateSuccess = true;
			}else {
				$updateMessage = "Contact No already exist !";
			}
		}else {
			$updateMessage = "Email already exist. Please try with another email";
		}
		if($updateSuccess){
			$response['status'] 	= 1;
			$response['message'] 	= $updateMessage;
		}else {
			$response['status'] 	= 0;
			$response['message'] 	= $updateMessage;
		}
		session()->flash('response', $response);
		return redirect()->back();
	}
	
	public function showCreateDeviceForm()
	{
		return view('create-new-device');
	}
	
	public function createNewDevice(Request $request)
	{
		$response = [];
		$user = User::where('email', $request->device_name."_".$request->device_role.$request->user()->hotel_id)->first();
		if($user == null) {
			$user = new User();
			$user->name = $request->device_name;
			$user->email = $request->device_name."_".$request->device_role.$request->user()->hotel_id;
			$user->hotel_id = $request->user()->hotel_id;
			$user->role = $request->device_role;
			$user->password = bcrypt($request->device_password);
			$user->save();
		
			$response['status'] 	= 1;
			$response['message'] 	= 'Device Created Successfully. Id = '.$user->email;
			
			session()->flash('response', $response);
			return redirect()->route('manage-device');
		}else {
			$response['status'] 	= 0;
			$response['message'] 	= 'Name Already Exist. Please try with another Name';
		}
		session()->flash('response', $response);
		return redirect()->back();
	}
	
	
	public function manageDevices()
	{
		$devices = User::where('hotel_id', Auth::user()->hotel_id)->where('id','!=', Auth::user()->id)->orderBy('id', 'DESC')->whereIn('role', [3,4])->get();
		return view('manage-devices', compact('devices'));
	}
	
	public function editDevice($device_id)
	{
		$device = User::find($device_id);
		if($device) {
			return view('edit-device', compact('device'));
		}
		
		$response['status'] 	= 0;
		$response['message'] 	= 'Device not found!';
		session()->flash('response', $response);
		return redirect()->route('manage-device');
		// Try to take if-else response abstract so that I can change it anytime from one place.
	}
	
	public function submitDevice(Request $request)
	{
		$device = User::find($request->device_id);
		if($device) {
			$device->name 		= $request->device_name;
			$device->password	= $request->password == null ? $device->password : bcrypt($request->password);
			$device->save();

			$response['status'] 	= 1;
			$response['message'] 	= 'Device has been successfully updated!';

			session()->flash('response', $response);
			return redirect()->route('manage-device');
		}
		$response['status'] 	= 0;
		$response['message'] 	= 'Device not found!';
		session()->flash('response', $response);
		return redirect()->route('manage-device');
	}
	
	public function deleteDevice($device_id)
	{
		// Make it delete via POST request
		$device = User::find($device_id);
		if($device) {
			$device->delete();
			$response['status'] 	= 1;
			$response['message'] 	= $device->name. ' has been successfully deleted!';
		}
		else {
			$response['status'] 	= 0;
			$response['message'] 	= 'Device not found!';
		}
		session()->flash('response', $response);
		return redirect()->route('manage-device');
	}
	
	public function showOwnerProfile()
	{
		$user = User::find(Auth::user()->id);
		return view('owner-profile', compact('user'));
	}
	
	public function showOwnerProfileForm()
	{
		$user = User::find(Auth::user()->id);
		return view('edit-owner-profile', compact('user'));
	}
	public function submitOwnerProfile(Request $request)
	{
		$user = User::find(Auth::user()->id);
		$user->name 			= $request->name;
		// $user->address 			= $request->address;
		$user->save();

		$response['status'] 	= 1;
		$response['message'] 	= 'Your profile has been successfully updated';
		session()->flash('response', $response);
		return redirect()->back();
	}
	
	public function showChangePasswordForm()
	{
		return view('change-owner-password');
	}
	
	public function submitOwnerPassword(Request $request)
	{
		$user = User::find(Auth::user()->id);
		$user->password	= bcrypt($request->password);
		// $user->address 			= $request->address;
		$user->save();

		$response['status'] 	= 1;
		$response['message'] 	= 'Password has been successfully updated';
		session()->flash('response', $response);
		return redirect()->back();
	}
	
	
	public function manageEmployees()
	{
		$employees = Employee::where('hotel_id', Auth::user()->hotel_id)->where('delete_status', 0)->orderBy('id', 'DESC')->get();
		return view('manage-employees', compact('employees'));
	}
	
	public function showCreateEmployeeForm()
	{
		return view('add-employee');
	}
	
	public function submitAddEmployee(Request $request)
	{
		$response = [];
		$employee = Employee::where('email', $request->email)->where('hotel_id', Auth::user()->hotel_id)->where('delete_status', 0)->first();
		if($employee == null) {
			$employee = Employee::where('mobile_no', $request->mobile_no)->where('hotel_id', Auth::user()->hotel_id)->where('delete_status', 0)->first();
			if($employee == null) {
				$employee = new Employee();
				$employee->name = $request->name;
				$employee->email = $request->email;
				$employee->mobile_no = $request->mobile_no;
				$employee->hotel_id = Auth::user()->hotel_id;
				$employee->gender = $request->gender;
				$employee->save();
			
				$response['status'] 	= 1;
				$response['message'] 	= 'Employee Created Successfully.';
				
				session()->flash('response', $response);
				return redirect()->route('manage-employees');
			}else {
				$response['status'] 	= 0;
				$response['message'] 	= 'Mobile No Already Exist. Please try with another Mobile NO';
			}
		}else {
			$response['status'] 	= 0;
			$response['message'] 	= 'Email Already Exist. Please try with another Email';
		}
		session()->flash('response', $response);
		return redirect()->back();
	}
	
	public function editEmployee($employee_id)
	{
		$employee = Employee::find($employee_id);
		if($employee) {
			return view('edit-employee', compact('employee'));
		}
		
		$response['status'] 	= 0;
		$response['message'] 	= 'employee not found!';
		session()->flash('response', $response);
		return redirect()->route('manage-employees');
		// Try to take if-else response abstract so that I can change it anytime from one place.
	}
	
	public function submitEditEmployee(Request $request)
	{
		$response = [];
		$employee = Employee::where('email', $request->email)->where('hotel_id', Auth::user()->hotel_id)->where('delete_status', 0)->where('id', '!=', $request->employee_id)->first();
		if($employee == null) {
			$employee = Employee::where('mobile_no', $request->mobile_no)->where('hotel_id', Auth::user()->hotel_id)->where('delete_status', 0)->where('id', '!=', $request->employee_id)->first();
			if($employee == null) {
				$employee = Employee::find($request->employee_id);
				$employee->name = $request->name;
				$employee->email = $request->email;
				$employee->mobile_no = $request->mobile_no;
				$employee->gender = $request->gender;
				$employee->save();
			
				$response['status'] 	= 1;
				$response['message'] 	= 'Employee Updated Successfully.';
				
				session()->flash('response', $response);
				return redirect()->route('manage-employees');
			}else {
				$response['status'] 	= 0;
				$response['message'] 	= 'Mobile No Already Exist. Please try with another Mobile NO';
			}
		}else {
			$response['status'] 	= 0;
			$response['message'] 	= 'Email Already Exist. Please try with another Email';
		}
		session()->flash('response', $response);
		return redirect()->back();
	}
	
	public function deleteEmployee($employee_id)
	{
		// Make it delete via POST request
		$employee = Employee::where('id', $employee_id)->where('hotel_id', Auth::user()->hotel_id)->first();
		if($employee) {
			$employee->delete_status = 1;
			$employee->save();
			$response['status'] 	= 1;
			$response['message'] 	= $employee->name. ' has been successfully deleted!';
		}
		else {
			$response['status'] 	= 0;
			$response['message'] 	= 'Employee not found!';
		}
		session()->flash('response', $response);
		return redirect()->route('manage-employees');
	}
}
