<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\User;
use App\Hotel;
use App\FoodType;
use App\FoodItem;
use App\OrderDetail;
use App\OrderSummary;
use App\Employee;
use Auth;

class OwnerController extends Controller
{
    public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('hotel.named')->except(['showHotelDetailsForm', 'submitHotelDetails']);
	}
	
	public function dashboard(Request $request)
	{
		$dashboardData = array();
		$from_date = $request->from_date ?? Carbon::now()->startOfMonth();
		$to_date = $request->to_date ?? Carbon::now();
		$foodTypes = FoodType::where('status', 1)->where('hotel_id', Auth::user()->hotel_id)->get();
		if ($foodTypes->isEmpty()) {
			$dashboardData = [
							'message' 	=> 'List Not Found',
							'code'		=> 404,
							'status' 	=> false,
							'data'		=> array()
						];
		}else {
			$fromDate = Carbon::parse($from_date);
			$toDate = Carbon::parse($to_date);
			$toDate = $toDate->modify('+1 day');
			$totalRevenueToday = 0;
			//$totalRevenue = OrderSummary::where('status', 2)->where('hotel_id', $request->user()->hotel_id)->whereBetween('updated_at',[$fromDate, $toDate])->sum('total_price');
			$totalRevenue = OrderSummary::where('status', 2)->where('hotel_id', Auth::user()->hotel_id)->sum('total_price');
			//$totalOrders = OrderSummary::where('status', 2)->where('hotel_id', $request->user()->hotel_id)->whereBetween('updated_at',[$fromDate, $toDate])->count();
			$totalOrders = OrderSummary::where('status', 2)->where('hotel_id', Auth::user()->hotel_id)->count();
			$orderSummaries = OrderSummary::where('status', 2)->where('hotel_id', Auth::user()->hotel_id)->whereBetween('updated_at',[$fromDate, $toDate])->orderby('id', 'DESC')->get();
			$responseArray = array();
			foreach($orderSummaries as $orderSummary){
				$totalRevenueToday += $orderSummary->total_price;
				$orderDetails = $orderSummary->orderDetails;
				$orderDetailsResponse = array();
				$employeeName = "";
				$preparationTime = "";
				foreach($orderDetails as $orderDetail){
					$foodItem = FoodItem::find($orderDetail->item_id);
					$employee = Employee::find($orderDetail->prepared_by);
					
					$startTime = Carbon::parse($orderDetail->prepare_start_time);
					$endTime = Carbon::parse($orderDetail->prepare_end_time);
					
					$preparationTime =  gmdate('H:i:s', $startTime->diffInSeconds($endTime));
					$employeeName = $employee->name ?? "";
					array_push($orderDetailsResponse, array(
													'id'			=> $orderDetail->id,
													'submenu_id'	=> $orderDetail->item_id,
													'submenu_name'	=> $foodItem->name ?? "",
													'submenu_image'	=> asset($foodItem->image ?? ""),
													'item_count'	=> $orderDetail->item_count,
													'item_price'	=> "".$orderDetail->price,
													'total_price'	=> "".$orderDetail->price*$orderDetail->item_count,
													'discount'		=> "".$orderDetail->discount,
													'extra_ingredients'=> $orderDetail->extraIngredients,
													));
				}
				array_push($responseArray, array(
							'token_id'			=> $orderSummary->id,
							'token_no'			=> $orderSummary->daily_token,
							'order_no'			=> $orderSummary->order_no,
							'note'				=> $orderSummary->note,
							'order_date'		=> $orderSummary->updated_at->format('Y-m-d h:m:s'),
							'total_price'		=> $orderSummary->total_price,
							'prepared_by'		=> $employeeName,
							'preparation_time'	=> $preparationTime,
							'order_details' 	=> $orderDetailsResponse,
						));
			}
			$dashboardData = [
							'message' 		=> 'List Fetched Successfully',
							'code'			=> 200,
							'status' 		=> true,
							'data'			=> $responseArray,
							'total_orders'	=> $totalOrders,
							'total_revenue'	=> round($totalRevenue, 2),
							'today_revenue'	=> round($totalRevenueToday, 2),
						];
		}
		//$hotels = Hotel::where('id', Auth::user()->hotel_id)->get();
		return view('dashboard', compact('dashboardData', 'from_date', 'to_date'));
	}
	public function employeePerformance(Request $request)
	{
		$performanceData = array();
		$from_date = $request->from_date ?? Carbon::now()->startOfMonth();
		$to_date = $request->to_date ?? Carbon::now();
		$fromDate = Carbon::parse($from_date);
		$toDate = Carbon::parse($to_date);
		$toDate = $toDate->modify('+1 day');
		$employees = Employee::where('delete_status', 0)->where('hotel_id', Auth::user()->hotel_id)->orderBy('name')->get();
		if($employees->isEmpty()){
			$performanceData = [
						'message' 	=> 'List Not Found',
						'code'		=> 404,
						'status' 	=> false,
						'data'		=> array()
					];
		}else{
			$employeePerformance = array();
			foreach($employees as $employee){
				$totalTakenOrders = 0;
				$totalSpentTime = "00:00:00";
				$orderSummaries = OrderSummary::where('hotel_id', $request->user()->hotel_id)->whereBetween('updated_at',[$fromDate, $toDate])->get();
				foreach($orderSummaries as $orderSummary){
					$orderDetail = $orderSummary->orderDetails->where('prepared_by', $employee->id)->first();
					if($orderDetail != null){
						$startTime = Carbon::parse($orderDetail->prepare_start_time);
						$endTime = Carbon::parse($orderDetail->prepare_end_time);
						$preparationTime = $startTime->diffInSeconds($endTime);
						
						$totalSpentTime = date("H:i:s",strtotime($totalSpentTime) + $preparationTime);
						
						$totalTakenOrders++;
					}
					
				}
				$totalOrders = OrderDetail::where('prepared_by', $employee->id)->count();
				array_push($employeePerformance, array(
								'id'				=> $employee->id, 
								'name'				=> $employee->name,
								'totalTakenOrders'	=> $totalTakenOrders,
								'totalSpentTime'	=> $totalSpentTime,
							));
			}
			$performanceData = [
						'message' 		=> 'List Fetched Successfully',
						'code'			=> 200,
						'status' 		=> true,
						'data'			=> $employeePerformance,
					];
		}
		return view('employee-performance', compact('performanceData', 'from_date', 'to_date'));
	}
	
}
