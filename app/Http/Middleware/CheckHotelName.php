<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Hotel;

class CheckHotelName
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		try {
		  if(is_null(Hotel::select('name')->where('id', Auth::user()->hotel_id)->first()->name)){
			return redirect()->route('enter-hotel-details');
		  }
		}
		catch (\Exception $e) {
		  return redirect()->route('add-new-owner');
		}
		
        return $next($request);
    }
}
