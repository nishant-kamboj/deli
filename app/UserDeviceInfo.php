<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDeviceInfo extends Model
{
    protected $table = 'user_device_info';
}
