<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FoodCategory extends Model
{
    public function foodItems()
    {
        return $this->hasMany('App\FoodItem', 'category_id', 'id');
    }
	public function foodTypes()
	{
		return $this->belongsToMany('App\FoodType', 'food_type_items', 'item_id', 'type_id');
	}
}
