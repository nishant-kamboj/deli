<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderSummary extends Model
{
	protected $table = 'order_summarys';
	
	public function orderDetails()
    {
        return $this->hasMany('App\OrderDetail', 'summary_id', 'id');
    }
}
