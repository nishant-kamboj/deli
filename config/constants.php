<?php

const SUPER_ADMIN 		= 1;
const HOTEL_OWNER 		= 2;
const KITCHER_USER 		= 3;
const CUSTOMER 			= 4;

const DEFAULT_ROLE_WEB 	= 2;

return array('role_names' => array("role1" => "Super Admin", "role2" => "Owner", "role3" => "Kitchen", "role4" => "Customer"));