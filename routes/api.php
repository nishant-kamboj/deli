<?php
use Illuminate\Http\Request;
Route::group([
], function () {
    Route::post('signup', 'Api\SignUpController@signup');
    Route::post('User_Login', 'Api\LoginLogoutController@UserLogin');
    Route::post('User_ForgetPassword', 'Api\OwnerController@UserForgetPassword');
    Route::post('Device_Login', 'Api\LoginLogoutController@DeviceLogin');
    Route::get('send_noti', 'Api\OrderController@sendNoti');
  
    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('User_GetUsers', 'Api\OwnerController@GetUsers');
        Route::post('User_EditProfile', 'Api\OwnerController@UserEditProfile');
        Route::post('User_EditHotel', 'Api\OwnerController@UserEditHotel');
        Route::post('User_UpdatePassword', 'Api\OwnerController@UserUpdatePassword');
        Route::post('User_Dashboard', 'Api\OwnerController@UserDashboard');
        Route::post('Device_CreateDevice', 'Api\OwnerController@CreateDevice');
        Route::post('Device_UpdateDevice', 'Api\OwnerController@UpdateDevice');
        Route::post('Device_DeleteDevice', 'Api\OwnerController@DeleteDevice');
        Route::post('Ingredient_CreateIngredient', 'Api\IngredientController@CreateIngredient');
        Route::post('Ingredient_GetIngredients', 'Api\IngredientController@GetIngredients');
        Route::post('Ingredient_Delete', 'Api\IngredientController@DeleteIngredient');
        Route::post('SubMenu_CreateSubMenu', 'Api\SubMenuController@CreateSubMenu');
        Route::post('SubMenu_GetSubMenus', 'Api\SubMenuController@GetSubMenus');
        Route::post('SubMenu_PublishUnpublish', 'Api\SubMenuController@PublishUnpublishSubMenu');
        Route::post('SubMenu_Delete', 'Api\SubMenuController@DeleteSubMenu');
        Route::post('SubMenu_EditSubMenu', 'Api\SubMenuController@EditSubMenu');
        Route::post('Menu_CreateMenu', 'Api\MenuController@CreateMenu');
        Route::get('Menu_GetMenus', 'Api\MenuController@GetMenus');
        Route::post('Menu_EditMenu', 'Api\MenuController@EditMenu');
        Route::post('Menu_Delete', 'Api\MenuController@DeleteMenu');
        Route::post('Order_AddToCart', 'Api\OrderController@AddToCart');
        Route::post('Order_ViewSummary', 'Api\OrderController@ViewSummary');
        Route::post('Order_DeleteFromCart', 'Api\OrderController@DeleteFromCart');
        Route::post('Order_ViewOrders', 'Api\OrderController@ViewOrders');
        Route::post('Order_ConfirmOrder', 'Api\OrderController@ConfirmOrder');
        Route::post('Order_IncDecItemCount', 'Api\OrderController@incDecItemCount');
        Route::post('Order_RemoveExtraIngredient', 'Api\OrderController@RemoveExtraIngredient');
        Route::post('Order_assignEmployee', 'Api\OrderController@assignEmployee');
        Route::post('Category_CreateCategory', 'Api\CategoryController@CreateCategory');
        Route::post('Category_GetCategories', 'Api\CategoryController@GetCategories');
        Route::post('Category_PublishCategory', 'Api\CategoryController@publishUnpublishCategory');
        Route::post('Category_Delete', 'Api\CategoryController@DeleteCategory');
        Route::post('Category_EditCategory', 'Api\CategoryController@EditCategory');
        Route::get('Send_Notification', 'Api\FirebaseController@SendNotification');
		Route::get('Employee_getEmployees', 'Api\EmployeeController@getEmployees');
		Route::post('Employee_employeePerformance', 'Api\EmployeeController@employeePerformance');
		
        Route::get('Logout', 'Api\LoginLogoutController@Logout');
        Route::get('user', 'Api\AuthController@user');
		
    });
});