@extends('layouts.admin-app')
@section('page-style')
<style>
	select:invalid {
	  height: 0px !important;
	  opacity: 0 !important;
	  margin-top: -10px;
	  display: flex !important;
	}

</style>
@endsection
@section('page-content')
<div class="right_col" role="main">
<div class="">

@if($response = session('response'))
<div class="alert @if($response['status']) alert-success @else alert-danger @endif" style="margin-top:60px">
	{{ $response['message'] }}
</div>
@endif

  <div class="page-title">
	
  </div>
  <div class="clearfix"></div>
  <div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
	  <div class="x_panel">
		<div class="x_title">
		  <h2>Create Category</h2>
		  <div class="clearfix"></div>
		</div>
		<div class="x_content">
		  <br />
		  <form method="POST" action="{{ route('submit-new-category') }}" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
		  @csrf
			<div class="form-group">
			  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="category-name">Category Name <span class="required">*</span>
			  </label>
			  <div class="col-md-6 col-sm-6 col-xs-12">
				<input name="category_name" type="text" id="category-name" required="required" class="form-control col-md-7 col-xs-12">
			  </div>
			</div>
			<div class="form-group">
			  <label class="control-label col-md-3 col-sm-3 col-xs-12">Select Menu <span class="required">*</span></label>
			  <div class="col-md-6 col-sm-6 col-xs-12">
				<select name="menu_ids[]" type="text" id="select-menu" class="form-control col-md-7 col-xs-12" required multiple data-placeholder="Choose Menus...">
				@foreach($menus as $menu)
					<option value="{{ $menu->id }}">{{ $menu->name }}</option>
				@endforeach
				</select>
			  </div>
			</div>
			<div class="ln_solid"></div>
			<div class="form-group">
			  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
				<button type="reset" class="btn btn-primary">Reset</button>
				<button type="submit" class="btn btn-success" id="submit-btn">Submit</button>
			  </div>
			</div>

		  </form>
		</div>
	  </div>
	</div>
  </div>

  <script type="text/javascript">
	$(document).ready(function() {
	  $('#birthday').daterangepicker({
		singleDatePicker: true,
		calender_style: "picker_4"
	  }, function(start, end, label) {
		console.log(start.toISOString(), end.toISOString(), label);
	  });
	  
		$("#select-menu").chosen();
	});
	
  </script>


  </div>
@endsection

@section('page-scripts')
	<script>
		$("#food-item").change(function() {
			var food_item_id = $(this).val();
			$("#submit-btn").prop('disabled', true);
			$("#submit-btn").html("Please wait...");

			$.ajax({
				url: '{{ route('get-linked-menus') }}',
				type: 'POST',
				dataType: 'json',
				data: { _token: '{{ csrf_token() }}', food_item_id: food_item_id },
			})
			.done(function(response) {
				$("#submit-btn").prop('disabled', false);
				$("#submit-btn").html("Submit");
				$("input").prop('checked', false);
				for(i=0; i<response.length; i++) {
					$("input[value='"+response[i]+"']").prop('checked', true);
				}
				// $("#already-linked-with").val(response);
			})
			.fail(function() {
				alert("Something went wrong, please try again later.")
			// })
			// .always(function() {
			// 	console.log("complete");
			});
			
		});
	</script>
@endsection
