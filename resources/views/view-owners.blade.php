@extends('layouts.admin-app')

@section('page-content')
<div class="right_col" role="main">

<div class="">

@if($response = session('response'))
<div class="alert @if($response['status']) alert-success @else alert-danger @endif" style="margin-top:60px">
	{{ $response['message'] }}
</div>
@endif

  <div class="page-title">
	
  </div>
  <div class="clearfix"></div>

  <div class="row">
	<div class="col-md-12">
	  <div class="x_panel">
		<div class="x_title">
		  <h2>Owners</h2>
		  <ul class="nav navbar-right panel_toolbox">
		  </ul>
		  <div class="clearfix"></div>
		</div>
		<div class="x_content">
		@if(!$owners->isEmpty())
		  <!-- start project list -->
		  <table class="table table-striped projects">
			<thead>
			  <tr>
				<!--<th style="width: 1%">#</th>-->
				<th>Name</th>
				<th>E-Mail</th>
				<th>Mobile No</th>
				<th>Hotel Name</th>
				<th style="width: 20%">Action</th>
			  </tr>
			</thead>
			<tbody>
			@foreach($owners as $owner)
			  <tr>
				<!--<td>#</td>-->
				<td>
					{{ $owner->name }}
				</td>
				<td>
					{{ $owner->email }}
				</td>
				<td>
					{{ $owner->mobile_no }}
				</td>
				<td>
					{{ $owner->hotel->name ?? "" }}
				</td>
				<td>
				  <a href="{{ route('edit-owner', $owner->id) }}" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
				</td>
			  </tr>
			@endforeach
			</tbody>
		  </table>
		  <!-- end project list -->
		@else
			<div class="" role="main">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<h2>List Not Found</h2>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		@endif
		</div>
	  </div>
	</div>
  </div>
</div>
@endsection

@section('page-scripts')
	<script>
		function submitChangeAdminStatusForm(admin_id, status) {
			$("#admin-id").val(admin_id);
			$("#status").val(status);
			$("#change-admin-status-form").submit();
		}
	</script>
@endsection
