@extends('layouts.admin-app')

@section('page-content')
<div class="right_col" role="main">

<div class="">

@if($response = session('response'))
<div class="alert @if($response['status']) alert-success @else alert-danger @endif" style="margin-top:60px">
	{{ $response['message'] }}
</div>
@endif

  <div class="page-title">
	
  </div>
  <div class="clearfix"></div>

  <div class="row">
	<div class="col-md-12">
	  <div class="x_panel">
		<div class="x_title">
		<div class="row">
			<h2>Hotels</h2>
			<ul class="nav navbar-right panel_toolbox">
				<a href="{{ route('add-new-owner') }}" class="btn btn-info btn-xs">Add Hotel</a>
			</ul>
			</div>
			<div class="row">
				<div class="col-sm-4 col-sm-offset-4">
				<input type="text" id="searchForInTable"class="form-control" placeholder="Search for..." style="border-radius: 50px; margin-bottom: 5px">
				</div>
			</div>
		  <div class="clearfix"></div>
		</div>
		<div class="x_content">
		@if(!$hotels->isEmpty())
		  <!-- start project list -->
		  <table class="table table-striped projects">
			<thead>
			  <tr>
				<!--<th style="width: 1%">#</th>-->
				<th>Name</th>
				<th>E-Mail</th>
				<th>Contact No</th>
				<th>Owner Name</th>
				<th style="width: 20%">Select Hotel</th>
			  </tr>
			</thead>
			<tbody>
			@foreach($hotels as $hotel)
			<?php $owner = App\User::where('hotel_id', $hotel->id)->where('id', '!=', Auth::user()->id)->first(); ?>
			  <tr class="hotelsTableTr">
				<!--<td>#</td>-->
				<td>
					{{ $hotel->name }}
				</td>
				<td>
					{{ $hotel->email }}
				</td>
				<td>
					{{ $hotel->contact_no }}
				</td>
				<td>
					{{ $owner->name ?? "" }}
				</td>
				<td>
				  <a href="{{ route('select-hotel-submit', $hotel->id) }}" class="btn btn-info btn-xs"><i class="fa fa-hand-pointer-o"></i> Select </a>
				</td>
			  </tr>
			@endforeach
			</tbody>
		  </table>
		  <!-- end project list -->
		@else
			<div class="" role="main">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<h2>List Not Found</h2>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		@endif
		</div>
	  </div>
	</div>
  </div>
</div>
@endsection

@section('page-scripts')
	<script>
		$(document).ready(function(){
		  $("#searchForInTable").on("keyup", function() {
			var value = $(this).val().toLowerCase();
			$(".hotelsTableTr").filter(function() {
			  $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
			});
		  });
		});
	</script>
	<script>
		function submitChangeAdminStatusForm(admin_id, status) {
			$("#admin-id").val(admin_id);
			$("#status").val(status);
			$("#change-admin-status-form").submit();
		}
	</script>
@endsection
