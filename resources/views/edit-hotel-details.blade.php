@extends('layouts.admin-app')

@section('page-content')
<div class="right_col" role="main">
<div class="">

@if($response = session('response'))
<div class="alert @if($response['status']) alert-success @else alert-danger @endif" style="margin-top:60px">
	{{ $response['message'] }}
</div>
@endif

  <div class="page-title">
	
  </div>
  <div class="clearfix"></div>
  <div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
	  <div class="x_panel">
		<div class="x_title">
		@if($hotel->name == null && $hotel->email == null && $hotel->contact_no == null)
			<h2>Add Hotel</h2>
		@else
			<h2>Update Hotel</h2>
		@endif
		  <div class="clearfix"></div>
		</div>
		<div class="x_content">
		  <br />
		  <form method="POST" action="{{ route('submit-hotel-details') }}" id="demo-form2" enctype="multipart/form-data" data-parsley-validate class="form-horizontal form-label-left">
		  @csrf
			<div class="form-group">
			  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="hotel-name">Hotel Name <span class="required">*</span>
			  </label>
			  <div class="col-md-6 col-sm-6 col-xs-12">
				<input name="hotel_name" pattern="[a-zA-Z\s]*" title="Please enter alphabets only." type="text" value="{{ $hotel->name }}" id="hotel-name" required="required" class="form-control col-md-7 col-xs-12">
			  </div>
			</div>
			<div class="form-group">
			  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email Address <span class="required">*</span>
			  </label>
			  <div class="col-md-6 col-sm-6 col-xs-12">
				<input name="email" type="email" value="{{ $hotel->email }}" id="email" pattern="[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="Please enter valid Email" required="required" class="form-control col-md-7 col-xs-12">
			  </div>
			</div>
			<div class="form-group">
			  <label for="contact-no" class="control-label col-md-3 col-sm-3 col-xs-12">Contact No <span class="required">*</span></label>
			  <div class="col-md-6 col-sm-6 col-xs-12">
				<input name="contact_no" pattern="[0-9]{6,15}" title="Please enter valid Contact No" type="tel" value="{{ $hotel->contact_no }}" id="contact-no" class="form-control col-md-7 col-xs-12" required>
			  </div>
			</div>
			<div class="form-group">
			  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">Address <span class="required">*</span>
			  </label>
			  <div class="col-md-6 col-sm-6 col-xs-12">
				<textarea name="address" id="address" class="form-control col-md-7 col-xs-12" required>{{ $hotel->address }}</textarea>
			  </div>
			</div>
			<div class="form-group">
			  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="hotel-image">Image 
			  </label>
			  <div class="col-md-4 col-sm-4 col-xs-8">
				<input type="file" name="image" id="hotel-image" class="form-control col-md-7 col-xs-12" style="height: 45px" accept="image/x-png,image/jpeg">
			  </div>
			  <div class="col-md-2 col-sm-2 col-xs-4">
				<img src="{{ asset($hotel->image) }}" width='80' height="45"/>
			  </div>
			</div>
			<div class="ln_solid"></div>
			<div class="form-group">
			  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
				<button type="reset" class="btn btn-primary">Reset</button>
				<button type="submit" class="btn btn-success">Submit</button>
			  </div>
			</div>

		  </form>
		</div>
	  </div>
	</div>
  </div>

  <script type="text/javascript">
	$(document).ready(function() {
	  $('#birthday').daterangepicker({
		singleDatePicker: true,
		calender_style: "picker_4"
	  }, function(start, end, label) {
		console.log(start.toISOString(), end.toISOString(), label);
	  });
	});
  </script>



  </div>
@endsection

@section('page-scripts')
	<script>
		function submitChangeUserStatusForm(user_id, status) {
			$("#user-id").val(user_id);
			$("#status").val(status);
			$("#change-user-status-form").submit();
		}
	</script>
@endsection
