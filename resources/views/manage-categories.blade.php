@extends('layouts.admin-app')

@section('page-content')
<div class="right_col" role="main">

<div class="">

@if($response = session('response'))
<div class="alert @if($response['status']) alert-success @else alert-danger @endif" style="margin-top:60px">
	{{ $response['message'] }}
</div>
@endif

  <div class="page-title">
	
  </div>
  <div class="clearfix"></div>

  <div class="row">
	<div class="col-md-12">
	  <div class="x_panel">
		<div class="x_title">
		  <h2>Manage Category</h2>
		  <ul class="navbar-right">
			<a href="{{ route('create-new-category') }}" class="btn btn-info btn-xs">Add New Category</a>
			<!--<a href="{{ route('link-food-items') }}" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Link Category To Menu </a>-->
		  </ul>
		  <div class="clearfix"></div>
		</div>
		<div class="x_content">
		@if(!$foodCategories->isEmpty())
		  <!-- start project list -->
		  <table class="table table-striped projects">
			<thead>
			  <tr>
				<!--<th style="width: 1%">#</th>-->
				<th style="width: 12%">Name</th>
				<th style="width: 8%">Menus</th>
				<!--<th style="width: 8%">Status</th>-->
				<th style="width: 22%">Action</th>
			  </tr>
			</thead>
			<tbody>
			@foreach($foodCategories as $foodCategory)
			  <tr>
				<!--<td>#</td>-->
				<td>
					{{ $foodCategory->name }}
				</td>
				<td>
				@if($foodCategory->foodTypes->count() > 0)
					{{ implode(', ', $foodCategory->foodTypes->pluck('name')->toArray()) }}
				@else
					No Menu is Linked
				@endif
				</td>
				<!--<td>
					@if($foodCategory->status)
					  <button type="button" class="btn btn-success btn-xs" style="width:70px">Active</button>
					@else
					  <button type="button" class="btn btn-danger btn-xs" style="width:70px">Blocked</button>
					@endif
				</td>-->
				<td>
					@if($foodCategory->status)
						<a 
							href="{{ route('change-category-status') }}"
							onclick="event.preventDefault(); submitChangeFoodItemStatusForm({{ $foodCategory->id }}, 0)"
							class="btn btn-danger btn-xs" style="width:85px">
							<i class="fa fa-ban"></i> UnPublish 
						</a>
					@else

						<a 
							href="{{ route('change-category-status') }}"
							onclick="event.preventDefault(); submitChangeFoodItemStatusForm({{ $foodCategory->id }}, 1)"
							class="btn btn-success btn-xs" style="width:85px">
							<i class="fa fa-check"></i> Publish 
						</a>
					@endif
				
				  <a href="{{ route('edit-category', $foodCategory->id) }}" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
				  <a href="{{ route('delete-food-category', $foodCategory->id) }}" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
				</td>
			  </tr>
			@endforeach
			</tbody>
			
		  <form id="change-food-item-status-form" action="{{ route('change-category-status') }}"   method="POST" style="display: none;">
			{{ csrf_field() }}
			<input type="hidden" name="food_category_id" id="food-category-id">
			<input type="hidden" name="status" id="status">
          </form>
			
		  </table>
		  <!-- end project list -->
		@else
			<div class="" role="main">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<h2>List Not Found</h2>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		@endif
		</div>
	  </div>
	</div>
  </div>
</div>
@endsection

@section('page-scripts')
	<script>
		function submitChangeFoodItemStatusForm(food_item_id, status) 
		{
			$("#food-category-id").val(food_item_id);
			$("#status").val(status);
			$("#change-food-item-status-form").submit();
		}
	</script>
@endsection
