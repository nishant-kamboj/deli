@extends('layouts.admin-app')

@section('page-style')
	<style>
		table td,th {
			text-align: center;
			padding: 10px;
			font-size: 16px;
		}
		table {
			width: 100%;
		}
		.total-orders-container {
			border-bottom: 1px solid;
			margin-bottom: 40px;
			padding-bottom: 10px;
		}
	</style>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">
@endsection

@section('page-content')
<div class="right_col" role="main">

<div class="">

@if($response = session('response'))
<div class="alert @if($response['status']) alert-success @else alert-danger @endif" style="margin-top:60px">
	{{ $response['message'] }}
</div>
@endif

  <div class="page-title">
	
  </div>
  <div class="clearfix"></div>

  <div class="row">
	<div class="col-md-12">
	  <div class="x_panel">
		<div class="x_title">
		
                <!-- Global Filter Control -->
                <div class="row">
                    <div class="col-xs-12">
                        <div class="panel panel-default">
<!--                            <div class="panel-heading" style="background-color: #7D021D;">
                                <font style="color: #fff;font-size: 16px;"><u>Filter Details</u></font>

                            </div>-->
							<form action="{{ route('employee-performance') }}">
                            <div class="panel-body">
                                <div class="col-sm-4 form-group">
                                    <label class="table-heading-color-custom"> Pick from Date: </label>
                                    <input type="text" id="date-from" name="from_date" class="form-control">
                                </div>
                                <div class="col-sm-4 form-group">
                                    <label class="table-heading-color-custom"> Pick to Date: </label>
                                    <input type="text" id="date-to" name="to_date" class="form-control">
                                </div>
                                <div class="form-group col-sm-4">
                                    <label> &nbsp </label>
                                    <button type="submit" 
                                            class="btn form-control btn-default" style="line-height: 5px !important;">Filter</button>
                                </div>
                            </div>
							</form>
                            <!-- /.panel-body -->
                        </div>
                    </div>
                </div>
                <!-- Global Filter Control End-->  
                
                
		</div>
		<div class="x_title">
		  <h2>Employee Performance</h2>
			<ul class="nav navbar-right panel_toolbox">
				<input type="text" id="searchForInTable"class="form-control" placeholder="Search for..." style="border-radius: 50px; margin-bottom: 5px">
			</ul>
		  <div class="clearfix"></div>
		</div>
		<div class="x_content">
		@if($performanceData['status'])
		  <!-- start project list -->
	 
	  <div class="row">
		
		<div class="x_content">
		  <table class="table">
			<thead>
			  <tr>
				<th>Name</th>
				<th>Order Taken</th>
				<th>Total Hours</th>
			  </tr>
			</thead>
			<tbody>
			@foreach($performanceData['data'] as $data)
			<tr class="employeePerformanceTableTr">
				<td>{{ $data['name'] }}</td>
				<td>{{ $data['totalTakenOrders'] }}</td>
				<td>{{ $data['totalSpentTime'] }}</td>
			</tr>
			@endforeach
			
			</tbody>
		  </table>
		</div>
		  <!-- end project list -->
		@else
			<div class="" role="main">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<h2>List Not Found</h2>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		@endif
		</div>
	  </div>
	</div>
  </div>
</div>
@endsection

@section('page-scripts')
	<script>
		$(document).ready(function(){
		  $("#searchForInTable").on("keyup", function() {
			var value = $(this).val().toLowerCase();
			$(".employeePerformanceTableTr").filter(function() {
			  $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
			});
		  });
		});
	</script>
	<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
	
	<script>
	$(document).ready(function() {
		$("#date-from").datepicker({
			dateFormat: "yy-mm-dd",
				onSelect: function(selected) {
					$("#date-to").datepicker("option","minDate", selected);
				}
			});
		$("#date-to").datepicker({
            dateFormat: "yy-mm-dd",
            minDate:new Date($("#from").val()),
            onSelect: function(selected) {}
        });
		var todayDate=new Date($.now());
        todayDate.setMonth(todayDate.getMonth(),1);
		@if($from_date)
			$("#date-from").datepicker("setDate", new Date('{{$from_date}}'));
		@else
			$("#date-from").datepicker("setDate", todayDate);
		@endif
		@if($to_date)
			$("#date-to").datepicker("setDate", new Date('{{$to_date}}'));
		@else
			$("#date-to").datepicker("setDate", new Date($.now()));
		@endif
	});
	</script>
@endsection
