@extends('layouts.admin-app')

@section('page-content')
<div class="right_col" role="main">
<div class="">

@if($response = session('response'))
<div class="alert @if($response['status']) alert-success @else alert-danger @endif" style="margin-top:60px">
	{{ $response['message'] }}
</div>
@endif

  <div class="page-title">
	
  </div>
  <div class="clearfix"></div>
  <div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
	  <div class="x_panel">
		<div class="x_title">
		  <h2>Add Owner</h2>
		  
		  <div class="clearfix"></div>
		</div>
		<div class="x_content">
		  <br />
		  <form method="POST" action="{{ route('submit-add-new-owner') }}" enctype="multipart/form-data" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
		  @csrf
		  
			<div class="form-group">
			  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="employee-name">Name <span class="required">*</span>
			  </label>
			  <div class="col-md-6 col-sm-6 col-xs-12">
				<input name="name" type="text" pattern="[a-zA-Z\s]*" title="Please enter alphabets only." id="employee-name" required="required" class="form-control col-md-7 col-xs-12">
			  </div>
			</div>
			<div class="form-group">
			  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email Address <span class="required">*</span>
			  </label>
			  <div class="col-md-6 col-sm-6 col-xs-12">
				<input name="email" type="email" pattern="[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="Please enter valid Email" id="email" required="required" class="form-control col-md-7 col-xs-12">
			  </div>
			</div>
			<div class="form-group">
			  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Mobile No <span class="required">*</span>
			  </label>
			  <div class="col-md-6 col-sm-6 col-xs-12">
				<input id="mobile_no" type="text" class="form-control{{ $errors->has('mobile_no') ? ' is-invalid' : '' }}" name="mobile_no" value="{{ old('mobile_no') }}" placeholder="Mobile No" required pattern="[0-9]{6,15}" title="Please Enter Valid Mobile No.">
			  </div>
			</div>
			<div class="form-group">
			  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Password <span class="required">*</span></label>
			  <div class="col-md-6 col-sm-6 col-xs-12">
				<input id="password1" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" pattern=".{6,}" title="Six or more characters" placeholder="Password" required>
			  </div>
			</div>
			<div class="form-group">
			  <label for="contact-no" class="control-label col-md-3 col-sm-3 col-xs-12">Confim Password <span class="required">*</span></label>
			  <div class="col-md-6 col-sm-6 col-xs-12">
				<input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" required>
			  </div>
			</div>
			
			<div class="ln_solid"></div>
			<div class="form-group">
			  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
				<button type="reset" class="btn btn-primary">Reset</button>
				<button type="submit" class="btn btn-success">Submit</button>
			  </div>
			</div>

		  </form>
		</div>
	  </div>
	</div>
  </div>
</div>
@endsection

@section('page-scripts')
<script>
	$( document ).ready(function(){
		function validatePassword(){
			if($("#password1").val() != $("#password-confirm").val()) {
				$("#password-confirm").get(0).setCustomValidity("Passwords Don't Match");
			} else {
				$("#password-confirm").get(0).setCustomValidity("");
			}
		}

		$("#password1").change(validatePassword);
		$("#password-confirm").keyup(validatePassword);
	});
</script>
@endsection
