@extends('layouts.admin-app')

@section('page-content')
<div class="right_col" role="main">
<div class="">

@if($response = session('response'))
<div class="alert @if($response['status']) alert-success @else alert-danger @endif" style="margin-top:60px">
	{{ $response['message'] }}
</div>
@endif

  <div class="page-title">
	
  </div>
  <div class="clearfix"></div>
  <div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
	  <div class="x_panel">
		<div class="x_title">
		  <h2>Update Food</h2>
		  
		  <div class="clearfix"></div>
		</div>
		<div class="x_content">
		  <br />
		  <form method="POST" action="{{ route('submit-edit-food-item') }}" enctype="multipart/form-data" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
		  @csrf
		  <input type="hidden" name="food_item_id" value="{{ $food_item->id }}">
			<div class="form-group">
			  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="item-name">Item Name <span class="required">*</span>
			  </label>
			  <div class="col-md-6 col-sm-6 col-xs-12">
				<input name="item_name" type="text" value="{{ $food_item->name }}" id="item-name" required="required" class="form-control col-md-7 col-xs-12">
			  </div>
			</div>
			<div class="form-group">
			  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description">Description <span class="required">*</span>
			  </label>
			  <div class="col-md-6 col-sm-6 col-xs-12">
				<textarea name="description" id="description" class="form-control col-md-7 col-xs-12">{{ $food_item->description }}</textarea>
			  </div>
			</div>
			<div class="form-group">
			  <label for="price" class="control-label col-md-3 col-sm-3 col-xs-12">Price <span class="required">*</span></label>
			  <div class="col-md-6 col-sm-6 col-xs-12">
				<input name="price" type="number" step="0.01" min="0" value="{{ $food_item->price }}" id="price" class="form-control col-md-7 col-xs-12" required>
			  </div>
			</div>
			
			<!-- for discount field -->
			<input name="discount" type="hidden" value="{{ $food_item->discount }}" id="discount" class="form-control col-md-7 col-xs-12">
			<!--<div class="form-group">
			  <label for="discount" class="control-label col-md-3 col-sm-3 col-xs-12">Discount (%)</label>
			  <div class="col-md-6 col-sm-6 col-xs-12">
				<input name="discount" type="number" value="{{ $food_item->discount }}" id="discount" class="form-control col-md-7 col-xs-12">
			  </div>
			</div>-->
			<div class="form-group">
			  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="food-ingredients">Select Extras  
			  </label>
			  <div class="col-md-6 col-sm-6 col-xs-12">
				<select name="food_ingredient_ids[]" type="text" id="food-ingredients"  class="form-control col-md-7 col-xs-12" multiple data-placeholder="Choose Extras...">
				@foreach($food_ingredients as $food_ingredient)
					<option value="{{ $food_ingredient->id }}" @if(in_array($food_ingredient->id, $linked_ingredient_ids)) selected @endif>{{ $food_ingredient->name }}</option>
				@endforeach
				</select>
			  </div>
			</div>
			<div class="form-group">
			  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="food-category">Select Category <span class="required">*</span>
			  </label>
				<div class="col-md-6 col-sm-6 col-xs-12">
				<select name="food_category_id" type="text" id="food-category" required="required" class="form-control col-md-7 col-xs-12">
				<option value=''>Select food Category</option>
				@foreach($foodCategories as $foodCategory)
				<?php 
					$menus = "";
					if($foodCategory->foodTypes->count() > 0) {
						$menus = ' ( '.implode(", ", $foodCategory->foodTypes()->pluck("name")->toArray()).' )';
						}else {
						$menus = ' ( No menu Linked )';
						}
				?>
					<option value="{{ $foodCategory->id }}" @if($foodCategory->id == $food_item->category_id) selected @endif>{{ $foodCategory->name.$menus}}</option>
				@endforeach
				</select>
			  </div>
			</div>
			
			<!-- <div class="form-group">
			  <label for="veg" class="control-label col-md-3 col-sm-3 col-xs-12">Food Type</label>
			  <div class="col-md-6 col-sm-6 col-xs-12">
			    <input name="type" type="radio" id="veg" value=1 @if($food_item->veg_status) checked @endif>
				Veg
			    <input name="type" type="radio" id="non-veg" value=0 @if(!$food_item->veg_status) checked @endif style="margin-left:20px">
				Non-Veg
			  </div>
			</div> -->
			<div class="form-group">
			  <label for="item-image" class="control-label col-md-3 col-sm-3 col-xs-12">Item Image</label>
			  <div class="col-md-4 col-sm-4 col-xs-8">
				<input name="image" type="file" id="item-image" style="height: 45px" accept="image/x-png,image/jpeg" class="form-control col-md-7 col-xs-12">
			  </div>
			  <div class="col-md-2 col-sm-2 col-xs-4">
				<img src="{{ asset($food_item->image) }}" width='80' height="45"/>
			  </div>
			</div>
			<div class="ln_solid"></div>
			<div class="form-group">
			  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
				<button type="reset" class="btn btn-primary">Reset</button>
				<button type="submit" class="btn btn-success">Submit</button>
			  </div>
			</div>

		  </form>
		</div>
	  </div>
	</div>
  </div>

  <script type="text/javascript">
	$(document).ready(function() {
	  $('#birthday').daterangepicker({
		singleDatePicker: true,
		calender_style: "picker_4"
	  }, function(start, end, label) {
		console.log(start.toISOString(), end.toISOString(), label);
	  });
	  
	  $("#food-ingredients").chosen();
	});
  </script>



  </div>
@endsection

@section('page-scripts')
	<script>
		function submitChangeUserStatusForm(user_id, status) {
			$("#user-id").val(user_id);
			$("#status").val(status);
			$("#change-user-status-form").submit();
		}
	</script>
@endsection
