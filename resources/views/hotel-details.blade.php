@extends('layouts.admin-app')
@section('page-style')
<style>

</style>
@endsection
@section('page-content')
<div class="right_col" role="main">

<div class="">
  <div class="page-title">
	<!--
		<div class="title_left">
			<h3>Hotel Details <small></small></h3>
		</div>
		-->
  </div>
  <div class="clearfix"></div>

  <div class="row">
	<div class="col-md-12">
	  <div class="x_panel">
		<div class="x_title">
		  <h2>Hotel Details</h2>
		  
		  <div class="clearfix"></div>
		</div>
		<div class="x_content">

		  <!-- start project list -->
		  <table class="table table-striped projects">
			<thead>
			  <tr>
				<!--<th style="width: 1%">#</th>-->
				<th style="width: 20%">Full Name</th>
				<th>Email</th>
				<th>Phone No.</th>
				<th>Created On</th>
				<th style="width:20%">Address</th>
				<th style="width:20%">Action</th>
			  </tr>
			</thead>
			<tbody>
			@foreach($hotels as $hotel)
			  <tr>
				<!--<td>#</td>-->
				<td>
					{{ $hotel->name }}
				</td>
				<td>
					{{ $hotel->email ?? "Unavailable" }}
				</td>
				<td>
					{{ $hotel->contact_no ?? "Unavailable"  }}
				</td>
				<td>
					{{ $hotel->created_at->diffForHumans() }}
				</td>
				<td>
					{{ $hotel->address ?? "Unavailable" }}
				</td>
				<td>
					<a href="{{ route('edit-hotel-details') }}" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit</a>
				</td>
			  </tr>
			@endforeach
			</tbody>
		  </table>

		  <!-- end project list -->

		</div>
	  </div>
	</div>
  </div>
</div>
@endsection

@section('page-scripts')
	<script>
		function submitChangeUserStatusForm(user_id, status) {
			$("#user-id").val(user_id);
			$("#status").val(status);
			$("#change-user-status-form").submit();
		}
	</script>
@endsection
