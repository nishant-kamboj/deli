@extends('layouts.admin-app')

@section('page-content')
<div class="right_col" role="main">

<div class="">

@if($response = session('response'))
<div class="alert @if($response['status']) alert-success @else alert-danger @endif" style="margin-top:60px">
	{{ $response['message'] }}
</div>
@endif

  <div class="page-title">
	
  </div>
  <div class="clearfix"></div>

  <div class="row">
	<div class="col-md-12">
	  <div class="x_panel">
		<div class="x_title">
		  <h2>Manage Extra</h2>
		  <ul class="nav navbar-right panel_toolbox">
			<a href="{{ route('add-food-ingredient') }}" class="btn btn-info btn-xs">Add Extra</a>
		  </ul>
		  <div class="clearfix"></div>
		</div>
		<div class="x_content">
		@if(!$food_ingredients->isEmpty())
		  <!-- start project list -->
		  <table class="table table-striped projects">
			<thead>
			  <tr>
				<!--<th style="width: 1%">#</th>-->
				<th>Name</th>
				<!--<th>Status</th>-->
				<th>Action</th>
			  </tr>
			</thead>
			<tbody>
			@foreach($food_ingredients as $food_ingredient)
			  <tr>
				<!--<td>#</td>-->
				<td>
					{{ $food_ingredient->name ?? "Unavailable" }}
				</td>
				<!--<td>
					@if($food_ingredient->status)
					  <button type="button" class="btn btn-success btn-xs" style="width:70px">Active</button>
					@else
					  <button type="button" class="btn btn-danger btn-xs" style="width:70px">Blocked</button>
					@endif
				</td>-->
				<td>
					@if($food_ingredient->status)
						<a 
							href="{{ route('change-food-ingredient-status') }}"
							onclick="event.preventDefault(); submitChangeFoodIngredientStatusForm({{ $food_ingredient->id }}, 0)"
							class="btn btn-danger btn-xs" style="width:85px">
							<i class="fa fa-ban"></i> UnPublish 
						</a>
					@else

						<a 
							href="{{ route('change-food-ingredient-status') }}"
							onclick="event.preventDefault(); submitChangeFoodIngredientStatusForm({{ $food_ingredient->id }}, 1)"
							class="btn btn-success btn-xs" style="width:85px">
							<i class="fa fa-check"></i> Publish 
						</a>
					@endif
				
				  <a href="{{ route('edit-food-ingredient', $food_ingredient->id) }}" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
				  <a href="{{ route('delete-food-ingredient', $food_ingredient->id) }}" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
				</td>
			  </tr>
			@endforeach
			</tbody>
			
		  <form id="change-food-ingredient-status-form" action="{{ route('change-food-ingredient-status') }}"   method="POST" style="display: none;">
			{{ csrf_field() }}
			<input type="hidden" name="food_ingredient_id" id="food-ingredient-id">
			<input type="hidden" name="status" id="status">
          </form>
			
		  </table>
		  <!-- end project list -->
		@else
			<div class="" role="main">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<h2>List Not Found</h2>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		@endif
		</div>
	  </div>
	</div>
  </div>
</div>
@endsection

@section('page-scripts')
	<script>
		function submitChangeFoodIngredientStatusForm(food_ingredient_id, status)
		{
			$("#food-ingredient-id").val(food_ingredient_id);
			$("#status").val(status);
			$("#change-food-ingredient-status-form").submit();
		}
	</script>
@endsection
