@extends('layouts.admin-app')

@section('page-content')
<div class="right_col" role="main">
<div class="">

@if($response = session('response'))
<div class="alert @if($response['status']) alert-success @else alert-danger @endif" style="margin-top:60px">
	{{ $response['message'] }}
</div>
@endif

  <div class="page-title">
	
  </div>
  <div class="clearfix"></div>
  <div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
	  <div class="x_panel">
		<div class="x_title">
		  <h2>Link Food Categories</h2>
		  
		  <div class="clearfix"></div>
		</div>
		<div class="x_content">
		  <br />
		  <form method="POST" action="{{ route('link-food-items-submit') }}" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
		  @csrf
			<div class="form-group">
			  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="food-item">Select Item<span class="required">*</span>
			  </label>
			  <div class="col-md-6 col-sm-6 col-xs-12">
				<select name="food_item_id" type="text" id="food-item" required="required" class="form-control col-md-7 col-xs-12">
				<option value=''>Select food item</option>
				@foreach($food_items as $food_item)
					<option value="{{ $food_item->id }}">{{ $food_item->name }}</option>
				@endforeach
				</select>
			  </div>
			</div>
			<!--div class="form-group">
			  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="already-linked-with">Already Linked With
			  </label>
			  <div class="col-md-6 col-sm-6 col-xs-12">
				<input class="form-control col-md-7 col-xs-12" id="already-linked-with" placeholder="Please select an Item to view already linked menu(s)" readonly>
			  </div>
			</div-->
			<div class="form-group">
			  <label class="control-label col-md-3 col-sm-3 col-xs-12">Select Menu</label>
			  <div class="col-md-6 col-sm-6 col-xs-12">
			  @foreach($menus as $menu)
			    <input name="menu_ids[]" type="checkbox" value="{{ $menu->id }}" style="margin-left:5px">
				{{ $menu->name }}
			  @endforeach
			  </div>
			</div>
			<div class="ln_solid"></div>
			<div class="form-group">
			  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
				<button type="reset" class="btn btn-primary">Reset</button>
				<button type="submit" class="btn btn-success" id="submit-btn">Submit</button>
			  </div>
			</div>

		  </form>
		</div>
	  </div>
	</div>
  </div>

  <script type="text/javascript">
	$(document).ready(function() {
	  $('#birthday').daterangepicker({
		singleDatePicker: true,
		calender_style: "picker_4"
	  }, function(start, end, label) {
		console.log(start.toISOString(), end.toISOString(), label);
	  });
	});
  </script>



  </div>
@endsection

@section('page-scripts')
	<script>
		$("#food-item").change(function() {
			var food_item_id = $(this).val();
			$("#submit-btn").prop('disabled', true);
			$("#submit-btn").html("Please wait...");

			$.ajax({
				url: '{{ route('get-linked-menus') }}',
				type: 'POST',
				dataType: 'json',
				data: { _token: '{{ csrf_token() }}', food_item_id: food_item_id },
			})
			.done(function(response) {
				$("#submit-btn").prop('disabled', false);
				$("#submit-btn").html("Submit");
				$("input").prop('checked', false);
				for(i=0; i<response.length; i++) {
					$("input[value='"+response[i]+"']").prop('checked', true);
				}
				// $("#already-linked-with").val(response);
			})
			.fail(function() {
				alert("Something went wrong, please try again later.")
			// })
			// .always(function() {
			// 	console.log("complete");
			});
			
		});
	</script>
@endsection
