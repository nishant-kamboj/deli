<!DOCTYPE html>
<?php 
	$hotel = App\Hotel::find(auth()->user()->hotel_id);
?>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

  <link href="{{ asset('admin/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('admin/fonts/css/font-awesome.min.css') }}" rel="stylesheet">
  <link href="{{ asset('admin/css/animate.min.css') }}" rel="stylesheet">
  <link href="{{ asset('admin/css/custom.css') }}" rel="stylesheet">
  <link href="{{ asset('admin/css/maps/jquery-jvectormap-2.0.3.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('admin/css/icheck/flat/green.css') }}" rel="stylesheet" />
  <link href="{{ asset('admin/css/floatexamples.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('admin/css/chosen.css') }}" rel="stylesheet" type="text/css" />

  <script src="{{ asset('admin/js/jquery.min.js') }}"></script>
  <script src="{{ asset('admin/js/nprogress.js') }}"></script>
 
	<style>
		.top-margin {
			margin-top: 55px !important;
		}
	</style>
	@yield('page-style')
</head>

<body class="nav-md">

  <div class="container body">


    <div class="main_container">

      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">

          <div class="navbar nav_title" style="border: 0;">
            <a href="{{ url('/') }}" class="site_title"><i class="fa fa-paw"></i> <span>{{ config('app.name', 'Laravel') }}</span></a>
          </div>
          <div class="clearfix"></div>

          <!-- menu prile quick info -->
          <div class="profile">
		  <div>
            <div class="profile_pic">
			
              <img src="{{ asset($hotel->image ?? "") }}" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
              <h2>{{ auth()->user()->name }}</h2>
              <span><small>{{ auth()->user()->email }}</small></span>
            </div>
			</div>
          </div>
          <!-- /menu prile quick info -->

          <br />

          <!-- sidebar menu -->
          <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

		  
		  
            <div class="menu_section top-margin">
              <ul class="nav side-menu" style="border-top: 1px solid #fff;">
			  @if(Auth::user()->role == 2)
				<li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard </a></li>
				<li><a href="{{ route('employee-performance') }}"><i class="fa fa-bar-chart"></i> Employee Performance </a></li>
			  @endif
			  @if(Auth::user()->role == 1)
				<li><a href="{{ route('view-hotel-details') }}"><i class="fa fa-info-circle"></i> View Hotel Details </a></li>
				<li><a href="{{ route('view-owners') }}"><i class="fa fa-users"></i> Owners </a></li>
				<li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard </a></li>
                <li><a href="{{ route('manage-device') }}"><i class="fa fa-tasks"></i> Manage Device </a></li>
				<?php 
					$hotel = App\Hotel::find(Auth::user()->hotel_id);
				?>
				@if(Auth::user()->hotel->hotel_type ?? 1 == 1)
					<li><a href="{{ route('manage-menus') }}"><i class="fa fa-tasks"></i> Manage Menus </a></li>
				@endif
                <li><a href="{{ route('manage-categories') }}"><i class="fa fa-tasks"></i> Manage Categories </a></li>
                <li><a href="{{ route('manage-food-items') }}"><i class="fa fa-tasks"></i> Manage Food Items </a></li>
                <li><a href="{{ route('manage-food-ingredients') }}"><i class="fa fa-tasks"></i> Manage Extras</a></li>
				<li><a href="{{ route('manage-employees') }}"><i class="fa fa-tasks"></i> Manage Employees</a></li>
			  @endif
              </ul>
            </div>

          </div>
          <!-- /sidebar menu -->

          <!-- /menu footer buttons -->
          <div class="sidebar-footer hidden-small" style="background-color:#172D44; height:25px">
            
      			<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
      				{{ csrf_field() }}
      			</form>
          </div>
          <!-- /menu footer buttons -->
        </div>
      </div>

      <!-- top navigation -->
      <div class="top_nav">

        <div class="nav_menu">
          <nav class="" role="navigation">
            <div class="nav toggle">
              <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
			  
              <li class="">
				<a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                  <img src="{{ asset($hotel->image ?? "") }}" alt="">{{ auth()->user()->name }}
                  <span class=" fa fa-angle-down"></span>
                </a>
                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                  @if(Auth::user()->role == 2)
					<li><a href="{{ route('view-owner-profile') }}">  Profile</a></li>
				  @endif
                  <li><a href="{{ route('owner-change-password') }}">  Change Password</a>
                  </li>
                  <li>
					<a href="{{ route('logout') }}"
					onclick="event.preventDefault();
						 document.getElementById('logout-form').submit();" title="Logout">
						<i class="fa fa-sign-out pull-right"></i> Log Out
					</a>
                  </li>
                </ul>
              </li>
			  
			@if(Auth::user()->role == 1)
			  <li role="presentation" class="dropdown">
				<a href="{{ route('select-hotel') }}" class="dropdown-toggle info-number">
                  <i class="fa fa-home"></i> 
                  <span class="">{{Auth::user()->hotel->name ?? ""}}</span>
				  <span class=" fa fa-angle-down"></span>
                </a>
			  </li>
            @endif
			</ul>
          </nav>
        </div>

      </div>
      <!-- /top navigation -->


      <!-- page content -->
	    @yield('page-content')
        <!-- footer content -->

        <!--<footer>
          <div class="copyright-info">
            <p class="pull-right">2018 All Rights Reserved. {{ config('app.copyright_text', 'Hotel Management System') }}</a>  
            </p>
          </div>
          <div class="clearfix"></div>
        </footer>-->
        <!-- /footer content -->
      </div>
      <!-- /page content -->

    </div>

  </div>

  <div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
  </div>


  <!-- bootstrap progress js -->
  <script src="{{ asset('admin/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('admin/js/progressbar/bootstrap-progressbar.min.js') }}"></script>
  <script src="{{ asset('admin/js/nicescroll/jquery.nicescroll.min.js') }}"></script>
  <!-- icheck -->
  <script src="{{ asset('admin/js/icheck/icheck.min.js') }}"></script>
  <!-- daterangepicker -->
  <script src="{{ asset('admin/js/moment/moment.min.js') }}"></script>
  <script src="{{ asset('admin/js/datepicker/daterangepicker.js') }}"></script>

  <script src="{{ asset('admin/js/custom.js') }}"></script>
  <script src="{{ asset('admin/js/chosen.jquery.min.js') }}"></script>

 <!-- /datepicker -->
  
  @yield('page-scripts')
  <!-- /footer content -->
</body>
</html>
