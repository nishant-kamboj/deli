@extends('layouts.admin-app')

@section('page-content')
<div class="right_col" role="main">
<div class="">

@if($response = session('response'))
<div class="alert @if($response['status']) alert-success @else alert-danger @endif" style="margin-top:60px">
	{{ $response['message'] }}
</div>
@endif

  <div class="page-title">
	
  </div>
  <div class="clearfix"></div>
  <div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
	  <div class="x_panel">
		<div class="x_title">
		  <h2>Update Menu</h2>
		  
		  <div class="clearfix"></div>
		</div>
		<div class="x_content">
		  <br />
		  <form method="POST" action="{{ route('submit-menu') }}" enctype="multipart/form-data" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
		  @csrf
		  <input type="hidden" name="menu_id" value="{{ $menu->id }}">
			<div class="form-group">
			  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="menu-name">Menu name <span class="required">*</span>
			  </label>
			  <div class="col-md-6 col-sm-6 col-xs-12">
				<input name="menu_name" type="text" value="{{ $menu->name }}" id="menu-name" required="required" class="form-control col-md-7 col-xs-12">
			  </div>
			</div>
			<div class="form-group">
			  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="starts-at">Starts At <span class="required">*</span>
			  </label>
			  <div class="col-md-6 col-sm-6 col-xs-12">
				<input name="starts_at" type="time" value="{{ $menu->starts_at }}" id="starts-at" required="required" class="form-control col-md-7 col-xs-12">
			  </div>
			</div>
			<div class="form-group">
			  <label for="ends-at" class="control-label col-md-3 col-sm-3 col-xs-12">Ends At <span class="required">*</span></label>
			  <div class="col-md-6 col-sm-6 col-xs-12">
				<input name="ends_at" type="time" value="{{ $menu->ends_at }}" id="ends-at" required="required" class="form-control col-md-7 col-xs-12">
			  </div>
			</div>
			<div class="form-group">
			  <label for="menu-image" class="control-label col-md-3 col-sm-3 col-xs-12">Menu Image</label>
			  <div class="col-md-4 col-sm-4 col-xs-8">
				<input name="image" type="file" id="menu-image" style="height: 45px" accept="image/x-png,image/jpeg" class="form-control col-md-7 col-xs-12">
			  </div>
			  <div class="col-md-2 col-sm-2 col-xs-4">
				<img src="{{ asset($menu->image) }}" width='80' height="45"/>
			  </div>
			</div>
			<!--div class="form-group">
			  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Address <span class="required">*</span>
			  </label>
			  <div class="col-md-6 col-sm-6 col-xs-12">
				<textarea name="address" class="form-control col-md-7 col-xs-12">{{ $menu->address }}</textarea>
			  </div>
			</div-->
			<div class="ln_solid"></div>
			<div class="form-group">
			  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
				<button type="reset" class="btn btn-primary">Reset</button>
				<button type="submit" class="btn btn-success">Submit</button>
			  </div>
			</div>

		  </form>
		</div>
	  </div>
	</div>
  </div>

  <script type="text/javascript">
	$(document).ready(function() {
	  $('#birthday').daterangepicker({
		singleDatePicker: true,
		calender_style: "picker_4"
	  }, function(start, end, label) {
		console.log(start.toISOString(), end.toISOString(), label);
	  });
	});
  </script>



  </div>
@endsection

@section('page-scripts')
	<script>
		function submitChangeUserStatusForm(user_id, status) {
			$("#user-id").val(user_id);
			$("#status").val(status);
			$("#change-user-status-form").submit();
		}
	</script>
@endsection
