@extends('layouts.admin-app')

@section('page-content')
<div class="right_col" role="main">

<div class="">

@if($response = session('response'))
<div class="alert @if($response['status']) alert-success @else alert-danger @endif" style="margin-top:60px">
	{{ $response['message'] }}
</div>
@endif

  <div class="page-title">
	
  </div>
  <div class="clearfix"></div>

  <div class="row">
	<div class="col-md-12">
	  <div class="x_panel">
		<div class="x_title">
		  <h2>Manage Employees</h2>
		  <ul class="nav navbar-right panel_toolbox">
			<a href="{{ route('add-employee') }}" class="btn btn-info btn-xs">Add Employee</a>
		  </ul>
		  <div class="clearfix"></div>
		</div>
		<div class="x_content">
		@if(!$employees->isEmpty())
		  <!-- start project list -->
		  <table class="table table-striped projects">
			<thead>
			  <tr>
				<!--<th style="width: 1%">#</th>-->
				<th>Name</th>
				<th>E-Mail</th>
				<th>Mobile No</th>
				<th style="width: 20%">Action</th>
			  </tr>
			</thead>
			<tbody>
			@foreach($employees as $employee)
			  <tr>
				<!--<td>#</td>-->
				<td>
					{{ $employee->name }}
				</td>
				<td>
					{{ $employee->email }}
				</td>
				<td>
					{{ $employee->mobile_no }}
				</td>
				<td>
				  <a href="{{ route('edit-employee', $employee->id) }}" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
				  <a href="{{ route('delete-employee', $employee->id) }}" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
				</td>
			  </tr>
			@endforeach
			</tbody>
		  </table>
		  <!-- end project list -->
		@else
			<div class="" role="main">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<h2>List Not Found</h2>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		@endif
		</div>
	  </div>
	</div>
  </div>
</div>
@endsection

@section('page-scripts')
	<script>
		function submitChangeAdminStatusForm(admin_id, status) {
			$("#admin-id").val(admin_id);
			$("#status").val(status);
			$("#change-admin-status-form").submit();
		}
	</script>
@endsection
