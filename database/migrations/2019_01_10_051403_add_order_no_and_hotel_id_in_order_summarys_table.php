<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrderNoAndHotelIdInOrderSummarysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_summarys', function (Blueprint $table) {
            $table->integer('hotel_id')->after('user_id');
			$table->bigInteger('order_no')->after('daily_token')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_summarys', function (Blueprint $table) {
            $table->dropColumn('hotel_id');
            $table->dropColumn('order_no');
        });
    }
}
